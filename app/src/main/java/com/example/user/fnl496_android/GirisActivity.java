package com.example.user.fnl496_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;



/**
 * Created by İlayda Şahiner on 18.03.2018.
 */

public class GirisActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs2 = getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs2.getString("ogrenci", "bulunamadi");
        if(ogrenciBilgisi.equals("bulunamadi")){
            Intent intent = new Intent(GirisActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(GirisActivity.this, NavigationActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
