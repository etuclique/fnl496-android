package com.example.user.fnl496_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapter.DogruYanlisCustomAdapter;
import adapter.EslemeCustomAdapter;
import objects.EslemeSoru;
import objects.dogruYanlisSoru;

/**
 * Created by feyzaiyi on 25.01.2018.
 */

public class SorularEslemeli extends ListFragment{

    int kitap_id;
    ArrayList<EslemeSoru> eslemeSorular;
    Ogrenci ogrenci;
    View p;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Gson gson = new Gson();
        SharedPreferences prefs = getActivity().getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

        Bundle bundle = this.getArguments();
        if (bundle != null)
        {
            kitap_id = bundle.getInt("id_of_clicked_book5", 0);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        eslemeSorular = new ArrayList<EslemeSoru>();
        p = inflater.inflate(R.layout.sorular_eslemeli,container,false);
        return p;

    }


    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         new GetEslemeList().execute();

         EslemeCustomAdapter adapter = new EslemeCustomAdapter(getActivity(), eslemeSorular);
         setListAdapter(adapter);

    }

    private class GetEslemeList extends AsyncTask<String, Void, String> {
        ArrayList<EslemeSoru> eslemeSorular;

        String urlEslemeli = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+kitap_id+"/esleme_questions";

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();

            String jsonStr = "";

            try {
                if(jsonStr.equals("")){
                    jsonStr = webreq.makeWebServiceCallGet(urlEslemeli);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa",jsonStr);

            eslemeSorular = ParseJsonForEslemeSorular(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (eslemeSorular == null) {
                Log.i("bosmu", "Issuelar arrayi boş");
            } else {
                EslemeCustomAdapter adapter = new EslemeCustomAdapter(getActivity(), eslemeSorular);
                setListAdapter(adapter);
            }
        }
    }

    private ArrayList<EslemeSoru> ParseJsonForEslemeSorular(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject esObj = jsonObj.getJSONObject(i);
                    EslemeSoru es = new EslemeSoru();
                    es.setSoruId(esObj.getInt("id"));
                     es.setSoruText(esObj.getString("soruText"));
                     es.setYandakiText(esObj.getString("yandakiText"));
                     es.setDogruHarf(esObj.getString("dogruHarf"));
                     es.setSayfaNo(esObj.getInt("sayfaNo"));

                    eslemeSorular.add(es);
                }

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return eslemeSorular;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

    }


}
