package com.example.user.fnl496_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapter.CevaplanmisSecmeCustomAdapter;
import adapter.DogruYanlisCustomAdapter;
import adapter.EslemeCustomAdapter;
import adapter.SecmeCustomAdapter;
import objects.EslemeSoru;
import objects.SecmeliSoru;
import objects.cevaplanmisSecSorular;
import objects.dogruYanlisSoru;

/**
 * Created by feyzaiyi on 25.01.2018.
 */

public class SorularSecmeli extends ListFragment{

    int kitap_id;
    ArrayList<cevaplanmisSecSorular> cevaplanmisSecSorular;
    Ogrenci ogrenci;
    View p;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null)
        {
            kitap_id = bundle.getInt("id_of_clicked_book5", 0);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        cevaplanmisSecSorular = new ArrayList<cevaplanmisSecSorular>();
        p = inflater.inflate(R.layout.sorular_secmeli,container,false);
        return p;

    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Gson gson = new Gson();
        SharedPreferences prefs = getActivity().getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

        new GetSecmeSorular().execute();
    }

    private class GetSecmeSorular extends AsyncTask<String, Void, String> {

        String urlSecmeli = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+kitap_id+"/ogrencinincevaplari";

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();

            String jsonStr = "";
            try {
                jsonStr = webreq.makeWebServiceCallGet(urlSecmeli);

            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa",jsonStr);

            cevaplanmisSecSorular = ParseJsonForCevaplanmisSecmeliSorular(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (cevaplanmisSecSorular == null) {
                Log.i("bosmu", "Issuelar arrayi boş");
            } else {
                CevaplanmisSecmeCustomAdapter adapter = new CevaplanmisSecmeCustomAdapter(getActivity(), cevaplanmisSecSorular);
                setListAdapter(adapter);
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

    }


    private ArrayList<cevaplanmisSecSorular> ParseJsonForCevaplanmisSecmeliSorular(String json) {

        if (json != null) {
            try {
                JSONObject jsonObj = new JSONObject(json);
                JSONArray dySorularArray = jsonObj.getJSONArray("secmeliSoruBilgileri");
                JSONArray dyOgrenciCevaplariArray = jsonObj.getJSONArray("studentSecmeliSoruCevaplari");

                for (int i = 0; i < dySorularArray.length(); i++) {
                    JSONObject soruObj = dySorularArray.getJSONObject(i);

                    cevaplanmisSecSorular soru = new cevaplanmisSecSorular();
                    soru.setSoru_id(soruObj.getInt("id"));
                    soru.setBook_id(soruObj.getInt("book_id"));
                    soru.setSoruText(soruObj.getString("soruText"));
                    soru.setaText(soruObj.getString("aText"));
                    soru.setbText(soruObj.getString("bText"));
                    soru.setcText(soruObj.getString("cText"));
                    soru.setdText(soruObj.getString("dText"));
                    soru.setDogruCevap(soruObj.getString("dogruCevap"));

                    JSONObject soruObj1 = dyOgrenciCevaplariArray.getJSONObject(i);
                    soru.setOgrenciCevap(soruObj1.getString("cevap"));
                    soru.setStudent_id(soruObj1.getInt("student_id"));

                    cevaplanmisSecSorular.add(soru);
                }

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                return null;
                // Do something to recover ... or kill the app.
            }
        }
        return cevaplanmisSecSorular;
    }


}
