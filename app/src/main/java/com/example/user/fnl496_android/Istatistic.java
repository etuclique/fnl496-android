package com.example.user.fnl496_android;

import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import adapter.KelimelerCustomAdapter;
import adapter.TextNoteCustomAdapter;
import objects.cevaplanmisDySorular;
import objects.cevaplanmisSecSorular;

public class Istatistic extends Fragment {

    TextView cevaplananSecSayisiGetir;
    TextView cevaplananSayisiGetir;
    TextView toplamDySayisiGetir;
    TextView toplamSecSayisiGetir;
    TextView dogruVerilenlerinSayisiGetir;
    TextView dogruVerilenSecSayisiGetir;
    TextView yanlisVerilenSecSayisiGetir;
    TextView yanlisVerilenlerinSayisiGetir;
    Button dogruYanlisSorular;
    Button secmeliSorular;

    int kitap_id;
    Ogrenci ogrenci;

    String dogruCevapSayisiDy,yanlisCevapSayisiDy,toplamCozulmusSoruSayisiDy,toplamSoruSayisiDy;
    String dogruCevapSayisiSec,yanlisCevapSayisiSec,toplamCozulmusSoruSayisiSec,toplamSoruSayisiSec;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        kitap_id = bundle.getInt("id_of_clicked_book", 0);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_istatistic, container, false);

    }


    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Gson gson = new Gson();
        SharedPreferences prefs2 = getActivity().getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs2.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());


        cevaplananSayisiGetir = (TextView) getActivity().findViewById(R.id.cevaplananSayisiGetir);
        cevaplananSecSayisiGetir = (TextView)  getActivity().findViewById(R.id.cevaplananSecSayisiGetir);
        toplamDySayisiGetir = (TextView)  getActivity().findViewById(R.id.toplamDySayisiGetir);
        toplamSecSayisiGetir = (TextView)  getActivity().findViewById(R.id.toplamSecSayisiGetir);
        dogruVerilenlerinSayisiGetir = (TextView)  getActivity().findViewById(R.id.dogruVerilenlerinSayisiGetir);
        dogruVerilenSecSayisiGetir = (TextView)  getActivity().findViewById(R.id.dogruVerilenSecSayisiGetir);
        yanlisVerilenSecSayisiGetir=(TextView)  getActivity().findViewById(R.id.yanlisVerilenSecSayisiGetir);
        yanlisVerilenlerinSayisiGetir =(TextView)  getActivity().findViewById(R.id.yanlisVerilenlerinSayisiGetir);

        dogruYanlisSorular = (Button)  getActivity().findViewById(R.id.DogruYanlisButton);
        secmeliSorular=(Button) getActivity().findViewById(R.id.SecmeliButton);
        dogruYanlisSorular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment_secilen= new SorularDogruYanlis();
                Bundle bundle = new Bundle();
                bundle.putInt("id_of_clicked_book5",kitap_id);
                fragment_secilen.setArguments(bundle);
                FragmentTransaction trans = getFragmentManager().beginTransaction();
                trans.replace(R.id.fragment1container,fragment_secilen,"fragment_secilen");
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();

            }
        });

        secmeliSorular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment_secilen= new SorularSecmeli();
                Bundle bundle = new Bundle();
                bundle.putInt("id_of_clicked_book5",kitap_id);
                fragment_secilen.setArguments(bundle);
                FragmentTransaction trans = getFragmentManager().beginTransaction();
                trans.replace(R.id.fragment1container,fragment_secilen,"fragment_secilen");
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();
            }
        });

        new GetTestSonuclari().execute();
    }


    private class GetTestSonuclari extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/" + kitap_id + "/testistatistikler");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            ParseJsonForTestSonuclari(jsonStr);
            return "a";
        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            cevaplananSayisiGetir.setText(toplamCozulmusSoruSayisiDy);
            cevaplananSecSayisiGetir.setText(toplamCozulmusSoruSayisiSec);
            toplamDySayisiGetir.setText(toplamSoruSayisiDy);
            toplamSecSayisiGetir.setText(toplamSoruSayisiSec);
            dogruVerilenlerinSayisiGetir.setText(dogruCevapSayisiDy);
            dogruVerilenSecSayisiGetir.setText(dogruCevapSayisiSec);
            yanlisVerilenlerinSayisiGetir.setText(yanlisCevapSayisiDy);
            yanlisVerilenSecSayisiGetir.setText(yanlisCevapSayisiSec);
        }

    }


    private void  ParseJsonForTestSonuclari(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject soruObj = jsonObj.getJSONObject(i);

                    if(soruObj.getString("soru_type").equals("dogruYanlisQuestion")){
                        dogruCevapSayisiDy=String.valueOf(soruObj.getInt("dogruCevapSayisi"));
                        yanlisCevapSayisiDy=String.valueOf(soruObj.getInt("yanlisCevapSayisi"));
                        toplamCozulmusSoruSayisiDy=String.valueOf(soruObj.getInt("toplamCozulmusSoruSayisi"));
                        toplamSoruSayisiDy=String.valueOf(soruObj.getInt("toplamSoruSayisi"));
                    }

                    if(soruObj.getString("soru_type").equals("secmeliQuestion")){
                        yanlisCevapSayisiSec=String.valueOf(soruObj.getInt("yanlisCevapSayisi"));
                        dogruCevapSayisiSec=String.valueOf(soruObj.getInt("dogruCevapSayisi"));
                        toplamCozulmusSoruSayisiSec=String.valueOf(soruObj.getInt("toplamCozulmusSoruSayisi"));
                        toplamSoruSayisiSec=String.valueOf(soruObj.getInt("toplamSoruSayisi"));
                    }
                }

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }

    }


}
