package com.example.user.fnl496_android;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class GecisKelimelerFragment extends Fragment {

    int kitap_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_gecis_kelimeler_fragment,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null)
        {
            kitap_id = bundle.getInt("id_of_clicked_book", 0);
        }

        Intent i = new Intent(getActivity(),KelimelerTabbed.class);
        i.putExtra("id_of_clicked_book",kitap_id);
        startActivity(i);


    }
}
