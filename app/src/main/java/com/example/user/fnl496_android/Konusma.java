package com.example.user.fnl496_android;

import android.graphics.Bitmap;

/**
 * Created by İlayda Şahiner on 08.03.2018.
 */

public class Konusma {

    String created_at;
    int konusma_id;
    int teacher_id;
    int student_id;
    String konu;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getKonusma_id() {
        return konusma_id;
    }

    public void setKonusma_id(int konusma_id) {
        this.konusma_id = konusma_id;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getKonu() {
        return konu;
    }

    public void setKonu(String konu) {
        this.konu = konu;
    }
}
