package com.example.user.fnl496_android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.io.IOException;
import java.util.ArrayList;
import adapter.CustomPagerAdapter;
import adapter.TextNoteCustomAdapter;
import objects.EslemeSoru;
import objects.SecmeliSoru;
import objects.dogruYanlisSoru;

public class Main2Activity extends AppCompatActivity {

    WebView mWebView;
    int kitap_id;
    String kitap_adi;
    String secilenKelime = "";
    String paramsForListeyeEkle = "";
    String paramsForConversationCreate = "";
    String paramsForMessageCreate = "";
    ArrayList<String> pages;
    CustomPagerAdapter adapter;
    CustomViewPager pager;
    ImageView locButton;
    ArrayList<String> notlar;
    int sayfaNo;
    int position;
    SharedPreferences defaults;
    SharedPreferences.Editor editor;
    Ogrenci ogrenci;
    Ogretmen ogretmen;
    Kitap a;
    ArrayList<dogruYanlisSoru> sorular1;
    ArrayList<EslemeSoru> sorular2;
    ArrayList<SecmeliSoru> sorular3;
    int sayfaNoS;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kitap_fragment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        kitap_id = getIntent().getExtras().getInt("id_of_clicked_book", 0);
        kitap_adi = getIntent().getExtras().getString("name_of_clicked_book", "yok");
        position = getIntent().getExtras().getInt("position_of_clicked_book", 0);
        pages = new ArrayList<String>();
        notlar = new ArrayList<String>();


        Gson gson = new Gson();
        SharedPreferences prefs2 = getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs2.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

        SharedPreferences pref3 = getSharedPreferences("OgretmenData", Context.MODE_PRIVATE);
        String ogretmenBilgisi = pref3.getString("ogretmen", "bulunamadi");
        ogretmen = gson.fromJson(ogretmenBilgisi, new TypeToken<Ogretmen>() {
        }.getType());

        pager = (CustomViewPager) findViewById(R.id.myViewPager);


        SharedPreferences prefs = getSharedPreferences("Kitap"+position, Context.MODE_PRIVATE);
        String kitapBilgi = prefs.getString("kitap"+position, "isim bulunamadi");
        if(kitapBilgi.equals("isim bulunamadi")){
            new GetKitapIcerik().execute();
        }else{
            pages = ParsePages(prefs.getString("kitap"+position, "isim bulunamadi"));
            adapter = new CustomPagerAdapter(Main2Activity.this, pages);
            pager.setAdapter(adapter);
        }


        defaults = PreferenceManager.getDefaultSharedPreferences(getApplication());
        editor = defaults.edit();

        sorular1 = new ArrayList<dogruYanlisSoru>();
        sorular2 = new ArrayList<EslemeSoru>();
        sorular3 = new ArrayList<SecmeliSoru>();

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                sayfaNoS = position+1; // sayfa kontrolünü o sayfaya daha gelmeden yapmak için +2

                pager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.none);
                new GetKitaptakiSayfaBazliDySorularList().execute();

                new GetKitaptakiSayfaBazliEsSorularList().execute();

                new GetKitaptakiSayfaBazliSecSorularList().execute();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main2_bar, menu);

        locButton = (ImageView) menu.findItem(R.id.action_test).getActionView();
        locButton.setImageResource(R.drawable.exam);
        locButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getApplicationContext(), SayfaBazliSorularActivity.class);
                intent1.putExtra("current_page_no", pager.getCurrentItem()+1);
                intent1.putExtra("id_of_clicked_book",kitap_id);
                startActivity(intent1);
            }
        });
        return true;
    }

   @Override
    protected void onResume() {
        super.onResume();

       new GetKitaptakiSayfaBazliDySorularList().execute();

       new GetKitaptakiSayfaBazliEsSorularList().execute();

       new GetKitaptakiSayfaBazliSecSorularList().execute();

    }

    private class GetKitapIcerik extends AsyncTask<String, Void, String> {

        String jsonStr = null;
        ProgressDialog progressDialog;
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Main2Activity.this);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage("Kitap İçeriği hazırlanıyor...");
            progressDialog.show();

        }
        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();

            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/books/" + kitap_id + "/kitapicerik");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            ParseJsonForKitapIcerik(jsonStr);
            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            progressDialog.dismiss();
            pages = ParsePages(a.getHtmlIcerik());
            adapter = new CustomPagerAdapter(Main2Activity.this, pages);
            pager.setAdapter(adapter);


        }
    }

    private void ParseJsonForKitapIcerik(String json) {
        if (json != null) {
            try {
                JSONObject jObj = new JSONObject(json);
                a = new Kitap();
                a.setKitap_id(jObj.getInt("id"));
                a.setHtmlIcerik(jObj.getString("kitapHtmlIcerik"));

                SharedPreferences preferences = getSharedPreferences("Kitap"+position, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("kitap"+position, a.getHtmlIcerik());
                editor.apply();

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_yazilinot:

                sayfaNo=pager.getCurrentItem()+1;
                new GetKitaptakiSayfaBazliNotlarList().execute();

                return true;
            case R.id.action_videonot:

                Intent intent = new Intent(getApplicationContext(), VideoNoteActivity.class);
                intent.putExtra("current_page_no", pager.getCurrentItem()+1);
                intent.putExtra("id_of_clicked_book",kitap_id);
                startActivity(intent);

                return true;

            case R.id.action_soundnot:

                Intent intent2 = new Intent(getApplicationContext(), SesNoteActivity.class);
                intent2.putExtra("current_page_no", pager.getCurrentItem()+1);
                intent2.putExtra("id_of_clicked_book",kitap_id);
                startActivity(intent2);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private class GetKitaptakiSayfaBazliNotlarList extends AsyncTask<String, Void, String> {

        String jsonStr = null;
        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();

            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/teachers/"+ogretmen.getOgretmen_id()+"/books/" + kitap_id + "/text_notes/"+sayfaNo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            notlar = ParseJsonForNotlar(jsonStr);
            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (jsonStr.equals("[]")) {
                Log.v("bosmu", "Issuelar arrayi boş");
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                alertDialogBuilder.setTitle("Öğretmenin Yazılı Notları:");
                alertDialogBuilder.setMessage(sayfaNo + " numaralı sayfada öğretmenin bir notu bulunmamaktadır.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();

            }else {
                notlar.clear();
                notlar = ParseJsonForNotlar(jsonStr);
                TextNoteCustomAdapter adapter = new TextNoteCustomAdapter(Main2Activity.this, notlar);
                new AlertDialog.Builder(Main2Activity.this)
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //TODO - Code when list item is clicked (int which - is param that gives you the index of clicked item)
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setTitle("Öğretmenin Yazılı Notları:")
                        .setCancelable(false)
                        .show();
            }
        }
    }

    private ArrayList<String> ParseJsonForNotlar(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject soruObj = jsonObj.getJSONObject(i);
                    String not = soruObj.getString("yaziliNotIcerik");
                    notlar.add(not);
                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return notlar;
    }

    private ArrayList<String> ParsePages(String kitapIcerik) {

        String butunKitap = kitapIcerik;
        String page = "";
        int pageCount = 1;
        int index = butunKitap.indexOf(("<DIV id=\"page_1\">"));
        String baslangic = butunKitap.substring(0, index);

        while (butunKitap.indexOf("<DIV id=\"page_" + pageCount + "\">") != -1) {
            int index1 = butunKitap.indexOf("<DIV id=\"page_" + pageCount + "\">");
            int index2 = butunKitap.indexOf("<DIV id=\"page_" + (pageCount + 1) + "\">");
            if (index2 == -1) {
                int index3 = butunKitap.indexOf("</BODY>");
                page = baslangic + butunKitap.substring(index1, index3) + "</BODY>\n" +
                        "</HTML>";
            } else {
                page = baslangic + butunKitap.substring(index1, index2) + "</BODY>\n" +
                        "</HTML>";
            }
            //Log.v("asdas", "" + pageCount);
            pages.add(page);
            pageCount++;
        }

        return pages;
    }

    @Override
    public void onActionModeStarted(android.view.ActionMode mode) {
        final android.view.ActionMode a = mode;
        Menu menus = mode.getMenu();

        int menusize= menus.size();

        for(int i=0;i<menusize;i++)
                menus.removeItem(menus.getItem(0).getItemId());

       /* menus.removeItem(menus.getItem(1).getItemId());
        menus.removeItem(menus.getItem(1).getItemId());
        menus.removeItem(menus.getItem(1).getItemId());
        menus.removeItem(menus.getItem(1).getItemId());*/
        menus.add(0, 26, 0, "Anlamına Bak");
        menus.add(0, 25, 1, "Listeye Ekle");
        menus.add(0,27,2,"Öğretmene Mesaj Gönder");
        menus.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                View myView = pager.findViewWithTag("View" + pager.getCurrentItem());
                mWebView = (WebView) myView.findViewById(R.id.webview);
                mWebView.evaluateJavascript("(function(){return window.getSelection().toString()})()",
                        new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                secilenKelime = value.substring(1, value.length() - 1);
                                Log.v("SELECTION", "SELECTION: " + secilenKelime);
                                new GetKelimeninAnlami().execute();
                            }
                        });
                a.finish();
                return false;
            }
        });

        menus.getItem(1).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                View myView = pager.findViewWithTag("View" + pager.getCurrentItem());
                mWebView = (WebView) myView.findViewById(R.id.webview);
                mWebView.evaluateJavascript("(function(){return window.getSelection().toString()})()",
                        new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                secilenKelime = value.substring(1, value.length() - 1);
                                Log.v("SELECTION", "SELECTION: " + secilenKelime);
                                paramsForListeyeEkle = "kelimeText=" + secilenKelime;
                                new KelimeyiListeyeEkle().execute();
                            }
                        });
                a.finish();
                return false;
            }
        });

        menus.getItem(2).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                View myView = pager.findViewWithTag("View" + pager.getCurrentItem());
                mWebView = (WebView) myView.findViewById(R.id.webview);
                mWebView.evaluateJavascript("(function(){return window.getSelection().toString()})()",
                        new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                secilenKelime = value.substring(1, value.length() - 1);
                                Log.v("SELECTION", "SELECTION: " + secilenKelime);

                                final Dialog post_dialog = new Dialog(Main2Activity.this);
                                post_dialog.setContentView(R.layout.post_dialog);
                                post_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                post_dialog.show();
                                post_dialog.getWindow().setGravity(Gravity.CENTER);

                                final EditText mesajText= (EditText) post_dialog.findViewById(R.id.mesaj);
                                final TextView sayfano = (TextView) post_dialog.findViewById(R.id.sayfano);
                                sayfano.setText(""+(pager.getCurrentItem()+1));
                                final TextView kitap = (TextView) post_dialog.findViewById(R.id.kitapadi);
                                kitap.setText(kitap_adi);
                                final TextView secilenMetin = (TextView) post_dialog.findViewById(R.id.secilenalanmetin);
                                secilenMetin.setText(secilenKelime);

                                Button paylas = (Button) post_dialog.findViewById(R.id.buttongonder);
                                paylas.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String mesaj =mesajText.getText().toString();
                                        String konu = "\""+kitap.getText()+"\""+ " kitabının "+ sayfano.getText()+". sayfasında bulunan "+ "\""+secilenKelime+"\"" +" kısmı ile ilgili";
                                        paramsForConversationCreate = "konu=" + konu;
                                        paramsForMessageCreate = "body=" + mesaj + "&" + "sender=" +"true";
                                        mesajText.setText(konu);
                                        new MetniOgretmeneGonder().execute();
                                        post_dialog.dismiss();
                                    }
                                });

                                Button kapat = (Button) post_dialog.findViewById(R.id.buttonvazgec);
                                kapat.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        post_dialog.dismiss();
                                    }
                                });

                            }
                        });
                a.finish();

                return false;
            }
        });

        super.onActionModeStarted(mode);
    }

    private class MetniOgretmeneGonder extends AsyncTask<String, Void, String> {
        String conversation_id = "0";
        int responseCode = 0;

        @Override
        protected String doInBackground(String... params) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                conversation_id = webreq.makeWebServiceCallPost("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/teachers/"+ogretmen.getOgretmen_id()+"/conversations", paramsForConversationCreate);
                Log.v("conversation_id",conversation_id);
                if(!conversation_id.equals("0")){
                    responseCode = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/conversations/"+conversation_id+"/messages", paramsForMessageCreate);
                    Log.v("response", "" + responseCode);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "a";
        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (conversation_id.equals("0")) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                alertDialogBuilder.setTitle("Mesajın Gönderilmesi");
                alertDialogBuilder.setMessage("Mesajınız şuanda gönderilemiyor.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            }else{
                if (responseCode == 201) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                    alertDialogBuilder.setTitle("Mesajın Gönderilmesi");
                    alertDialogBuilder.setMessage("Mesajınız başarılı bir şekilde öğretmeninize gönderilmiştir. Mesajlarınıza Mesajlar sekmesinden ulaşabilirsiniz.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                    alertDialogBuilder.setTitle("Mesajın Gönderilmesi");
                    alertDialogBuilder.setMessage("Mesajınız şuanda gönderilemiyor.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                }
            }
        }
    }

    private class KelimeyiListeyeEkle extends AsyncTask<String, Void, String> {
        int responseCode = 0;

        @Override
        protected String doInBackground(String... params) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                responseCode = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/" + kitap_id + "/student_chosen_words", paramsForListeyeEkle);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("AAA", "" + responseCode);
            return "a";
        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (responseCode == 201 || responseCode == 500) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                alertDialogBuilder.setTitle("Kelimenin Listeye Eklenmesi");
                alertDialogBuilder.setMessage(secilenKelime + " kelimesi listenize eklenmiştir.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                alertDialogBuilder.setTitle("Kelimenin Listeye Eklenmesi");
                alertDialogBuilder.setMessage("Kelimeniz listeye eklenememiştir.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            }
        }
    }

    private class GetKelimeninAnlami extends AsyncTask<String, Void, String> {
        String htmlStr = null;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            try {
                htmlStr = webreq.makeWebServiceCallGet("https://www.seslisozluk.net/" + secilenKelime + "-nedir-ne-demek/");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "a";
        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);

            final Dialog anlam_dialog = new Dialog(Main2Activity.this);
            anlam_dialog.setContentView(R.layout.dialog_anlam);
            anlam_dialog.show();
            anlam_dialog.getWindow().setGravity(Gravity.CENTER);

            TextView baslik = (TextView) anlam_dialog.findViewById(R.id.baslik);
            baslik.setText(secilenKelime);
            TextView ilkanlam = (TextView) anlam_dialog.findViewById(R.id.ilkanlam);
            TextView ikincianlam = (TextView) anlam_dialog.findViewById(R.id.ikincianlam);
            TextView cumleTurkce = (TextView) anlam_dialog.findViewById(R.id.cumleTurkce);
            TextView cumleIng = (TextView) anlam_dialog.findViewById(R.id.cumleIng);
            TextView bir = (TextView) anlam_dialog.findViewById(R.id.bir);
            TextView iki = (TextView) anlam_dialog.findViewById(R.id.iki);
            TextView ornekbaslik = (TextView) anlam_dialog.findViewById(R.id.ornekbaslik);
            TextView tr = (TextView) anlam_dialog.findViewById(R.id.tr);
            TextView eng = (TextView) anlam_dialog.findViewById(R.id.eng);
            ikincianlam.setVisibility(View.VISIBLE);
            cumleTurkce.setVisibility(View.VISIBLE);
            cumleIng.setVisibility(View.VISIBLE);
            bir.setVisibility(View.VISIBLE);
            iki.setVisibility(View.VISIBLE);
            ornekbaslik.setVisibility(View.VISIBLE);
            tr.setVisibility(View.VISIBLE);
            eng.setVisibility(View.VISIBLE);

            Button cancel = (Button) anlam_dialog.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    anlam_dialog.cancel();
                }
            });

            Document doc = Jsoup.parse(htmlStr);
            Element first = doc.select("dd").first();
            if (first == null) {
                ilkanlam.setText("Bu sözcüğün Türkçe karşılığı bulunamamıştır.");
                ikincianlam.setVisibility(View.GONE);
                cumleTurkce.setVisibility(View.GONE);
                cumleIng.setVisibility(View.GONE);
                bir.setVisibility(View.GONE);
                iki.setVisibility(View.GONE);
                ornekbaslik.setVisibility(View.GONE);
                tr.setVisibility(View.GONE);
                eng.setVisibility(View.GONE);
            } else {
                ilkanlam.setText(first.select("a").text());
                Element second = doc.select("dd").get(1);
                ikincianlam.setText(second.select("a").text());
                if (first.select("q").first() == null) {
                    ornekbaslik.setVisibility(View.GONE);
                    tr.setVisibility(View.GONE);
                    eng.setVisibility(View.GONE);
                    cumleTurkce.setVisibility(View.GONE);
                    cumleIng.setVisibility(View.GONE);
                } else {
                    cumleTurkce.setText(first.select("q").first().text());
                    cumleIng.setText(first.select("q").last().text());
                }
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        sayfaNo=pager.getCurrentItem()+1;
        new OgrencininEnSonKaldigiSayfa().execute();

        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }

    private class OgrencininEnSonKaldigiSayfa extends AsyncTask<String, Void, String> {
        int jsonStr = 0;
        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();

            try {
                jsonStr = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+kitap_id+"/ensonkalinansayfa","istatistik1=" +sayfaNo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa",""+jsonStr);
            //    kitaplar= ParseJsonForBook(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (jsonStr ==201) {
                Log.v("okey", "Istatistik güncellendi.");
            } else {
                Log.v("error", "Istatistik güncellenemedi.");
            }
        }
    }


    private class GetKitaptakiSayfaBazliDySorularList extends AsyncTask<String, Void, String> {


        String url = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+ kitap_id +"/sayfa_dys/" + sayfaNoS;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = "";
            try {
                jsonStr = webreq.makeWebServiceCallGet(url);

            }
            catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);

            sorular1 = ParseJsonForDySorular(jsonStr);

            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            /*Log.v("secsize1",""+sorular1.size());
            if (sorular1.size()>0) {
                pager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.left);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                alertDialogBuilder.setMessage("Bu sayfa için testleriniz bulunmaktadır.\nBir sonraki sayfaya testi çözmeden geçemezsiniz.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        final Animation myAnim = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.test_animation);
                        locButton.startAnimation(myAnim);
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            } else {
                pager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.all);
            }
            sorular1.clear();*/
        }

    }

    private ArrayList<dogruYanlisSoru> ParseJsonForDySorular(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject dyObj = jsonObj.getJSONObject(i);
                    dogruYanlisSoru dy = new dogruYanlisSoru();
                    dy.setSoruId(dyObj.getInt("id"));

                    WebRequest webreq = new WebRequest();
                    String jsonStr = "";
                    try {
                        jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/students_dyquestions/" + dy.getSoruId());
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(jsonStr.equals("") ||jsonStr.equals(" ") || jsonStr.contains("[]") ){
                        //true
                        sorular1.add(dy);
                    }

                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return sorular1;
    }


    private class GetKitaptakiSayfaBazliEsSorularList extends AsyncTask<String, Void, String> {


        String url = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+ kitap_id +"/sayfa_ess/" + sayfaNoS;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = "";
            try {
                jsonStr = webreq.makeWebServiceCallGet(url);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);

            sorular2 = ParseJsonForEslemeSorular(jsonStr);

            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            /*Log.v("secsize2",""+sorular2.size());
            if (sorular2.size()>0) {
                pager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.left);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                alertDialogBuilder.setMessage("Bu sayfa için testleriniz bulunmaktadır.\nBir sonraki sayfaya testi çözmeden geçemezsiniz.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        final Animation myAnim = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.test_animation);
                        locButton.startAnimation(myAnim);
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            } else {
                pager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.all);
            }
            sorular2.clear();*/
        }

    }

    private ArrayList<EslemeSoru> ParseJsonForEslemeSorular(String json) {

        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject esObj = jsonObj.getJSONObject(i);
                    EslemeSoru es = new EslemeSoru();
                    es.setSoruId(esObj.getInt("id"));

                    WebRequest webreq = new WebRequest();
                    String jsonStr = "";
                    try {
                        jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/students_esquestions/" + es.getSoruId());

                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("aaaaa", jsonStr);

                    if(jsonStr.equals("") ||jsonStr.equals(" ") || jsonStr.contains("[]") ){
                        //true
                        sorular2.add(es);
                    }

                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return sorular2;
    }


    private class GetKitaptakiSayfaBazliSecSorularList extends AsyncTask<String, Void, String> {


        String url = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+ kitap_id +"/sayfa_secs/" + sayfaNoS;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = "";

            try {
                jsonStr = webreq.makeWebServiceCallGet(url);

            }catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);

            sorular3 = ParseJsonForSecmeSorular(jsonStr);

            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            //Log.v("secsize1",""+sorular1.size());
            //Log.v("secsize2",""+sorular2.size());
            //Log.v("secsize3",""+sorular3.size());
            if (sorular1.size()>0 ||sorular2.size()>0||sorular3.size()>0) {
                pager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.left);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                alertDialogBuilder.setMessage("Bu sayfa için testleriniz bulunmaktadır.\nBir sonraki sayfaya testi çözmeden geçemezsiniz.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        final Animation myAnim = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.test_animation);
                        locButton.startAnimation(myAnim);
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            } else {
                pager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.all);
            }
            sorular1.clear();
            sorular2.clear();
            sorular3.clear();
        }
    }


    private ArrayList<SecmeliSoru> ParseJsonForSecmeSorular(String json) {

        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject sObj = jsonObj.getJSONObject(i);
                    SecmeliSoru s = new SecmeliSoru();
                    s.setSoruId(sObj.getInt("id"));

                    WebRequest webreq = new WebRequest();
                    String jsonStr = "";
                    try {
                        jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/students_secquestions/" +s.getSoruId() );

                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("aaaaa", jsonStr);

                    if(jsonStr.equals("") ||jsonStr.equals(" ") || jsonStr.contains("[]") ){
                        //true
                        sorular3.add(s);
                    }

                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return sorular3;
    }



}
