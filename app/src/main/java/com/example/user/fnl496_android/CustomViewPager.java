package com.example.user.fnl496_android;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

import java.io.IOException;

import adapter.CustomPagerAdapter;

/**
 * Created by İlayda Şahiner on 20.02.2018.
 */

public class CustomViewPager  extends ViewPager{
    public enum SwipeDirection {
        all, left, right, none ;
    }

        private float initialXValue;
        private SwipeDirection direction;

        public CustomViewPager(Context context, AttributeSet attrs) {
            super(context, attrs);
            this.direction = SwipeDirection.all;
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if (this.IsSwipeAllowed(event)) {
                return super.onTouchEvent(event);
            }

            return false;
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent event) {
            if (this.IsSwipeAllowed(event)) {
                return super.onInterceptTouchEvent(event);
            }

            return false;
        }

        private boolean IsSwipeAllowed(MotionEvent event) {
            if (this.direction == SwipeDirection.all) return true;

            if (direction == SwipeDirection.none)//disable any swipe
                return false;

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                initialXValue = event.getX();
                return true;
            }

            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                try {
                    float diffX = event.getX() - initialXValue;
                    if (diffX > 0 && direction == SwipeDirection.right) {
                        // swipe from left to right detected
                        return false;
                    } else if (diffX < 0 && direction == SwipeDirection.left) {
                        // swipe from right to left detected
                        return false;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            return true;
        }

        public void setAllowedSwipeDirection(SwipeDirection direction) {
            this.direction = direction;
        }
    }