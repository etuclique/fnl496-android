package com.example.user.fnl496_android;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapter.KelimelerCustomAdapter;
import adapter.KitaplarCustomAdapter;
import adapter.OgretmeninKelimeleriAdapter;

public class OgretmeninKelimeleriFragment extends ListFragment {
    int kitap_id;
    ArrayList<Kelime> kelimeler;
    Ogretmen ogretmen;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Gson gson = new Gson();
        SharedPreferences pref3 = getActivity().getSharedPreferences("OgretmenData", Context.MODE_PRIVATE);
        String ogretmenBilgisi = pref3.getString("ogretmen", "bulunamadi");
        ogretmen = gson.fromJson(ogretmenBilgisi, new TypeToken<Ogretmen>() {
        }.getType());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            kitap_id = bundle.getInt("id_of_clicked_book", 0);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        kelimeler = new ArrayList<Kelime>();
        View view = inflater.inflate(R.layout.activity_ogretmenin_kelimeleri_fragment, container, false);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetKelimelerList().execute();

    }

    private class GetKelimelerList extends AsyncTask<String, Void, String> {
        ArrayList<Kelime> kelimeler;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/teachers/"+ogretmen.getOgretmen_id()+"/books/" + kitap_id + "/teacher_chosen_words");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);

            kelimeler = ParseJsonForKelimeler(jsonStr);

            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (kelimeler == null) {
                Log.i("bosmu", "Issuelar arrayi boş");
            } else {
                OgretmeninKelimeleriAdapter adapter = new OgretmeninKelimeleriAdapter(getActivity(), kelimeler,kitap_id);
                setListAdapter(adapter);

            }
        }


    }

    private ArrayList<Kelime> ParseJsonForKelimeler(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject kelimeObj = jsonObj.getJSONObject(i);
                    Kelime kelime = new Kelime();
                    kelime.setKelime_id(kelimeObj.getInt("id"));
                    kelime.setKelimeText(kelimeObj.getString("kelimetext"));
                    kelimeler.add(kelime);
                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return kelimeler;
    }


}
