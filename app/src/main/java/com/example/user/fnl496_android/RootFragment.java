package com.example.user.fnl496_android;

        import android.support.v4.app.Fragment;
        import android.os.Bundle;
        import android.support.v4.app.FragmentTransaction;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;

/**
 * Created by İlayda Şahiner on 14.07.2017.
 */

public class RootFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.root_fragment, container, false);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment1container, new KitaplarFragment());
        transaction.commit();

        return view;
    }
}