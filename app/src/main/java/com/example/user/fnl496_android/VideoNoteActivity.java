package com.example.user.fnl496_android;

import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapter.TextNoteCustomAdapter;

/**
 * Created by İlayda Şahiner on 25.02.2018.
 */

public class VideoNoteActivity extends ListActivity {

    ArrayList<String> notlar;
    int kitap_id;
    int sayfaNo;
    Ogretmen ogretmen;

    @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.notes_activity);
            notlar = new ArrayList<String>();

            kitap_id = getIntent().getExtras().getInt("id_of_clicked_book", 0);
            sayfaNo = getIntent().getExtras().getInt("current_page_no", 0);

        Gson gson = new Gson();
        SharedPreferences pref2 = getSharedPreferences("OgretmenData", Context.MODE_PRIVATE);
        String ogretmenBilgisi = pref2.getString("ogretmen", "bulunamadi");
        ogretmen = gson.fromJson(ogretmenBilgisi, new TypeToken<Ogretmen>() {
        }.getType());

            new GetKitaptakiSayfaBazliNotlarList().execute();
        }

        private class GetKitaptakiSayfaBazliNotlarList extends AsyncTask<String, Void, String> {

            String jsonStr = null;
            @Override
            protected String doInBackground(String... arg0) {

                WebRequest webreq = new WebRequest();
                try {
                    jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/teachers/"+ogretmen.getOgretmen_id()+"/books/" + kitap_id + "/video_notes/"+sayfaNo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.v("aaaaa", jsonStr);

                notlar = ParseJsonForNotlar(jsonStr);

                return "a";

            }

            @Override
            protected void onPostExecute(String requestresult) {
                super.onPostExecute(requestresult);

                if (jsonStr.equals("[]")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(VideoNoteActivity.this);
                    alertDialogBuilder.setTitle("Öğretmenin Video Notu");
                    alertDialogBuilder.setMessage("Bu sayfada öğretmenin video notu bulunmamaktadır.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                } else {
                    TextNoteCustomAdapter adapter = new TextNoteCustomAdapter(VideoNoteActivity.this, notlar);
                    setListAdapter(adapter);
                }
            }
        }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(VideoNoteActivity.this, VideoNote.class);
        intent.putExtra("video_adi",notlar.get(position));
        startActivity(intent);

    }

        private ArrayList<String> ParseJsonForNotlar(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject soruObj = jsonObj.getJSONObject(i);
                    String not = soruObj.getString("videoNotIcerik");
                    notlar.add(not);
                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return notlar;
    }
    }