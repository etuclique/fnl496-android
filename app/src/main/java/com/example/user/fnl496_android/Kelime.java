package com.example.user.fnl496_android;

/**
 * Created by İlayda Şahiner on 06.02.2018.
 */

public class Kelime {
    int kelime_id;
    String kelimeText;

    public int getKelime_id() {
        return kelime_id;
    }

    public void setKelime_id(int kelime_id) {
        this.kelime_id = kelime_id;
    }

    public String getKelimeText() {
        return kelimeText;
    }

    public void setKelimeText(String kelimeText) {
        this.kelimeText = kelimeText;
    }
}
