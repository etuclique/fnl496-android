package com.example.user.fnl496_android;

/**
 * Created by İlayda Şahiner on 15.01.2018.
 */

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import javax.net.ssl.HttpsURLConnection;


public class WebRequest {

   /* public String makeWebServiceCallGet(String url) throws IOException {
        URL obj = new URL(url);

        InputStream in = new BufferedInputStream(obj.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1!=(n=in.read(buf))) {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        byte[] response = out.toByteArray();
        return response.toString();
    }*/


    public String makeWebServiceCallGet(String url) throws IOException {


        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();

    }

    public int makeWebServiceCallPostWithResponseCode(String urladdress, String params) throws IOException {
        URL url;
        String response = "";
        int reqresponseCode = 0;
        try {
            //byte[] postData = params.getBytes(StandardCharsets.UTF_8);
            byte[] postData = params.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            url = new URL(urladdress);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setReadTimeout(4 * 15001);
            urlConnection.setConnectTimeout(4 * 15001);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            urlConnection.setUseCaches(false);
            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            wr.write(postData);

/*
            try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                wr.write(postData);
            }
*/
            reqresponseCode = urlConnection.getResponseCode();
            if (reqresponseCode == HttpsURLConnection.HTTP_OK || reqresponseCode == HttpURLConnection.HTTP_ACCEPTED) {
                System.out.println("Server returned HTTP " + urlConnection.getResponseCode()
                        + " " + urlConnection.getResponseMessage());
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reqresponseCode;
    }

    public String makeWebServiceCallPost(String urladdress, String params) throws IOException {
        URL url;
        String response = "";
        int reqresponseCode = 0;
        try {
            //byte[] postData = params.getBytes(StandardCharsets.UTF_8);
            byte[] postData = params.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            url = new URL(urladdress);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setReadTimeout(4 * 15001);
            urlConnection.setConnectTimeout(4 * 15001);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            urlConnection.setUseCaches(false);
            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            wr.write(postData);

/*
            try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                wr.write(postData);
            }
*/
            reqresponseCode = urlConnection.getResponseCode();
            if (reqresponseCode == HttpsURLConnection.HTTP_OK || reqresponseCode == HttpURLConnection.HTTP_CREATED) {
                System.out.println("Server returned HTTP " + urlConnection.getResponseCode()
                        + " " + urlConnection.getResponseMessage());
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public int makeWebServiceCallDelete(String url) throws IOException {


        URL urls = new URL(url);
        HttpURLConnection httpCon = (HttpURLConnection) urls.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestProperty(
                "Content-Type", "application/x-www-form-urlencoded");
        httpCon.setRequestMethod("DELETE");
        httpCon.connect();

        return httpCon.getResponseCode();

    }
}