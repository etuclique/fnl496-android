package com.example.user.fnl496_android;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.MediaController;
import android.widget.VideoView;

import java.util.ArrayList;

/**
 * Created by İlayda Şahiner on 25.02.2018.
 */

public class VideoNote extends AppCompatActivity {

        String videoPath;
        int position;
        VideoView videoView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.video_note);

            videoPath= getIntent().getExtras().getString("video_adi", "");

            videoView = (VideoView) findViewById(R.id.videoView);
            MediaController mediaController = new MediaController(this);
            mediaController.setAnchorView(videoView);
            videoView.setMediaController(mediaController);
            videoView.setVideoPath("https://teacherclique.herokuapp.com/assets/videos/"+videoPath);
            videoView.start();



        }

    }
