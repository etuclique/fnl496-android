package com.example.user.fnl496_android;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import adapter.ConversationsCustomAdapter;
import adapter.MessagesCustomAdapter;
import adapter.TextNoteCustomAdapter;


/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class ForumFragment extends ListFragment {

    ArrayList<Konusma> konusmalar;
    ArrayList<Message> mesajlar;
    int conversation_id;
    String paramsForMessageCreate = "";
    int clickedPosition;
    Ogrenci ogrenci;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mesajlar = new ArrayList<Message>();
        konusmalar = new ArrayList<Konusma>();
        return inflater.inflate(R.layout.forum_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Gson gson = new Gson();
        SharedPreferences prefs = getActivity().getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

        new GetOgrenciKonusmalarList().execute();
    }

    private class GetOgrenciKonusmalarList extends AsyncTask<String, Void, String> {
        ArrayList<Konusma> konusmalar;
        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/conversations/");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa",jsonStr);

            konusmalar = ParseJsonForKonusmalar(jsonStr);
            //    kitaplar= ParseJsonForBook(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (konusmalar == null) {
                Log.i("bosmu", "Issuelar arrayi boş");
            } else {
                ConversationsCustomAdapter adapter = new ConversationsCustomAdapter(getActivity(), konusmalar);
                setListAdapter(adapter);
            }
        }
    }

    private ArrayList<Konusma> ParseJsonForKonusmalar(String json) {

        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject konusmaObj = jsonObj.getJSONObject(i);
                    Konusma konusma = new Konusma();
                    konusma.setKonusma_id(konusmaObj.getInt("id"));
                    String created_at=konusmaObj.getString("created_at");
                    konusma.setCreated_at(formatCreatedAt(created_at));
                    konusma.setTeacher_id(konusmaObj.getInt("teacher_id"));
                    konusma.setStudent_id(konusmaObj.getInt("student_id"));
                    konusma.setKonu(konusmaObj.getString("konu"));

                    konusmalar.add(konusma);
                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return konusmalar;
    }

    private String formatCreatedAt(String createdAt){
        return createdAt.substring(8,10)+"-"+createdAt.substring(5,7)+"-"+createdAt.substring(0,4)+" "+createdAt.substring(11,19);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        clickedPosition=position;
        conversation_id = konusmalar.get(position).getKonusma_id();
        new GetKonusmaninMesajlariList().execute();
    }

    private class GetKonusmaninMesajlariList extends AsyncTask<String, Void, String> {
        String jsonStr = null;
        ArrayList<Message> mesajlar;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/conversations/"+conversation_id+"/messages");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            mesajlar = ParseJsonForMesajlar(jsonStr);
            //    kitaplar= ParseJsonForBook(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (jsonStr.equals("[]")) {
                Log.v("bosmu", "Mesajlar arrayi boş");
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setTitle("Öğretmenle Öğrencinin Mesajları");
                alertDialogBuilder.setMessage("Mesajlar şuanda gösterilememektedir.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();

            }else {
                mesajlar.clear();
                mesajlar = ParseJsonForMesajlar(jsonStr);
                final MessagesCustomAdapter adapter = new MessagesCustomAdapter(getActivity(), mesajlar);
                new AlertDialog.Builder(getActivity())
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //TODO - Code when list item is clicked (int which - is param that gives you the index of clicked item)
                            }
                        })
                        .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Yeni Mesaj Gönder", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final Dialog post_dialog = new Dialog(getActivity());
                                post_dialog.setContentView(R.layout.post_dialog2);
                                post_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                post_dialog.show();
                                post_dialog.getWindow().setGravity(Gravity.CENTER);

                                final EditText mesajText= (EditText) post_dialog.findViewById(R.id.mesaj2);

                                Button paylas = (Button) post_dialog.findViewById(R.id.buttongonder2);
                                paylas.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String mesaj =mesajText.getText().toString();
                                        paramsForMessageCreate = "body=" + mesaj + "&" + "sender=" +"true";
                                        new MesajiOgretmeneGonder().execute();
                                        post_dialog.dismiss();
                                        new GetKonusmaninMesajlariList().execute();
                                    }
                                });

                                Button kapat = (Button) post_dialog.findViewById(R.id.buttonvazgec2);
                                kapat.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        post_dialog.dismiss();
                                    }
                                });
                            }
                        })
                        .setTitle("KONU: "+konusmalar.get(clickedPosition).getKonu())
                        .setCancelable(false)
                        .show();
            }
        }

    }

    private ArrayList<Message> ParseJsonForMesajlar(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject mesajObj = jsonObj.getJSONObject(i);
                    Message mesaj = new Message();
                    mesaj.setMessage_id(mesajObj.getInt("id"));
                    mesaj.setConversation_id(mesajObj.getInt("conversation_id"));
                    mesaj.setBody(mesajObj.getString("body"));
                    mesaj.setSender(mesajObj.getBoolean("sender"));
                    mesajlar.add(mesaj);
                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return mesajlar;
    }

    private class MesajiOgretmeneGonder extends AsyncTask<String, Void, String> {
        int responseCode = 0;

        @Override
        protected String doInBackground(String... params) {

            WebRequest webreq = new WebRequest();
            try {
                responseCode = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/conversations/"+conversation_id+"/messages", paramsForMessageCreate);
                Log.v("response", "" + responseCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "a";
        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
                if (responseCode != 201) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setTitle("Mesajın Gönderilmesi");
                    alertDialogBuilder.setMessage("Mesajınız şuanda gönderilemiyor.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                }

        }
    }


}
