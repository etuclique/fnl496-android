package com.example.user.fnl496_android;

/**
 * Created by İlayda Şahiner on 15.03.2018.
 */

public class Ogrenci {

    private int ogrenci_id;
    private String kullaniciAdi;
    private int ogrenciFoto;
    private int sube_id;
    private String email;


    public int getOgrenci_id() {
        return ogrenci_id;
    }

    public void setOgrenci_id(int ogrenci_id) {
        this.ogrenci_id = ogrenci_id;
    }

    public String getKullaniciAdi() {
        return kullaniciAdi;
    }

    public void setKullaniciAdi(String kullaniciAdi) {
        this.kullaniciAdi = kullaniciAdi;
    }

    public int getOgrenciFoto() {
        return ogrenciFoto;
    }

    public void setOgrenciFoto(int ogrenciFoto) {
        this.ogrenciFoto = ogrenciFoto;
    }

    public int getSube_id() {
        return sube_id;
    }

    public void setSube_id(int sube_id) {
        this.sube_id = sube_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
