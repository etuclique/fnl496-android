package com.example.user.fnl496_android;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.widget.MediaController;
import java.io.IOException;

/**
 * Created by İlayda Şahiner on 28.02.2018.
 */

public class SesNote extends AppCompatActivity  implements MediaPlayer.OnPreparedListener, MediaController.MediaPlayerControl{

        private MediaPlayer mediaPlayer;
        private MediaController mediaController;
        String soundPath="";
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ses_note);

            soundPath= getIntent().getExtras().getString("sound_adi", "");
            String url = "https://teacherclique.herokuapp.com/assets/sounds/"+soundPath;
            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(url);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaController = new MediaController(this);
                mediaController.setMediaPlayer(this);
                mediaController.setAnchorView(findViewById(R.id.main_audio_view));
                mediaController.setEnabled(true);
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onStop() {
            super.onStop();
            mediaController.hide();
            mediaPlayer.stop();
            mediaPlayer.release();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            //the MediaController will hide after 3 seconds - tap the screen to make it appear again
            mediaController.show();
            return false;
        }

        //--MediaPlayerControl methods----------------------------------------------------
        public void start() {
            mediaPlayer.start();
        }

        public void pause() {
            mediaPlayer.pause();
        }

        public int getDuration() {
            return mediaPlayer.getDuration();
        }

        public int getCurrentPosition() {
            return mediaPlayer.getCurrentPosition();
        }

        public void seekTo(int i) {
            mediaPlayer.seekTo(i);
        }

        public boolean isPlaying() {
            return mediaPlayer.isPlaying();
        }

        public int getBufferPercentage() {
            return 0;
        }

        public boolean canPause() {
            return true;
        }

        public boolean canSeekBackward() {
            return true;
        }

        public boolean canSeekForward() {
            return true;
        }

        @Override
        public int getAudioSessionId() {
            return 0;
        }
        //--------------------------------------------------------------------------------

        public void onPrepared(final MediaPlayer mediaPlayer) {
            mediaPlayer.start();
        }
    }









