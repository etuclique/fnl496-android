package com.example.user.fnl496_android;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String paramsForConversationCreate = "";
    String paramsForMessageCreate = "";
    Ogrenci ogrenci;
    Ogretmen ogretmen;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.f, new RootFragment());
        transaction.commit();


        Gson gson = new Gson();
        SharedPreferences prefs = getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

        SharedPreferences pref2 = getSharedPreferences("OgretmenData", Context.MODE_PRIVATE);
        String ogretmenBilgisi = pref2.getString("ogretmen", "bulunamadi");
        ogretmen = gson.fromJson(ogretmenBilgisi, new TypeToken<Ogretmen>() {
        }.getType());


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout=navigationView.getHeaderView(0);

        TextView tv1=(TextView)  headerLayout.findViewById(R.id.kullaniciadi);
        tv1.setText(ogrenci.getKullaniciAdi());
        TextView tv2=(TextView) headerLayout.findViewById(R.id.email);
        tv2.setText(ogrenci.getEmail());

        TextView tv3=(TextView) headerLayout.findViewById(R.id.ogretmenadi);
        tv3.setText(ogretmen.getKullaniciAdi());

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        drawer.addDrawerListener(mDrawerToggle);
        drawer.postDelayed(new Runnable() {
            @Override
            public void run() {
                drawer.openDrawer(Gravity.LEFT);
            }
        }, 1000);
        mDrawerToggle.syncState();



        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_mesaj){
            final Dialog post_dialog = new Dialog(NavigationActivity.this);
            post_dialog.setContentView(R.layout.post_dialog3);
            post_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            post_dialog.show();
            post_dialog.getWindow().setGravity(Gravity.CENTER);

            final EditText mesajText= (EditText) post_dialog.findViewById(R.id.mesaj3);
            final EditText konuText= (EditText) post_dialog.findViewById(R.id.konu3);
            Button paylas = (Button) post_dialog.findViewById(R.id.buttongonder3);
            paylas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mesaj =mesajText.getText().toString();
                    String konu = konuText.getText().toString();
                    paramsForConversationCreate = "konu=" + konu;
                    paramsForMessageCreate = "body=" + mesaj + "&" + "sender=" +"true";
                    new MesajGonder().execute();
                    post_dialog.dismiss();

                }
            });

            Button kapat = (Button) post_dialog.findViewById(R.id.buttonvazgec3);
            kapat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    post_dialog.dismiss();
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment=null;

        int id = item.getItemId();

        if (id == R.id.nav_kitaplar) {
            loadFr(new RootFragment());

        } else if (id == R.id.nav_kelimeler) {
        //    loadFr(new RootFragment2());
            loadFr(new SecilenKitabinKelimeleriFragment());

        } else if (id == R.id.nav_forum) {
            loadFr(new RootFragment3());

        } else if (id == R.id.nav_istatistik) {
            loadFr(new RootFragment4());

        }else if (id == R.id.nav_logout) {
            SharedPreferences prefs = getSharedPreferences("Kitaplar", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();

            SharedPreferences prefs2 = getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
            editor = prefs2.edit();
            editor.clear();
            editor.commit();

            SharedPreferences prefs3 = getSharedPreferences("OgretmenData", Context.MODE_PRIVATE);
            editor = prefs3.edit();
            editor.clear();
            editor.commit();

            SharedPreferences prefs7 = getSharedPreferences("KitapSayisi", Context.MODE_PRIVATE);
            int kitapSayisi = prefs7.getInt("kitapsayisi",0);
            editor = prefs7.edit();
            editor.clear();
            editor.commit();
            for(int i=0;i<kitapSayisi;i++){
                prefs7 = getSharedPreferences("Kitap"+i, Context.MODE_PRIVATE);
                editor = prefs7.edit();
                editor.clear();
                editor.commit();

            }

            Intent loginscreen=new Intent(this,MainActivity.class);
            loginscreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loginscreen);
            this.finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFr(Fragment fragment) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.f, fragment);
        ft.commit();

    }

    private class MesajGonder extends AsyncTask<String, Void, String> {
        String conversation_id = "0";
        int responseCode = 0;

        @Override
        protected String doInBackground(String... params) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                conversation_id = webreq.makeWebServiceCallPost("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/teachers/"+ogretmen.getOgretmen_id()+"/conversations", paramsForConversationCreate);
                Log.v("conversation_id",conversation_id);
                if(!conversation_id.equals("0")){
                    responseCode = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/conversations/"+conversation_id+"/messages", paramsForMessageCreate);
                    Log.v("response", "" + responseCode);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "a";
        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (conversation_id.equals("0")) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NavigationActivity.this);
                alertDialogBuilder.setTitle("Mesajın Gönderilmesi");
                alertDialogBuilder.setMessage("Mesajınız şuanda gönderilemiyor.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            }else{
                if (responseCode == 201) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NavigationActivity.this);
                    alertDialogBuilder.setTitle("Mesajın Gönderilmesi");
                    alertDialogBuilder.setMessage("Mesajınız başarılı bir şekilde öğretmeninize gönderilmiştir. Mesajlarınıza Mesajlar sekmesinden ulaşabilirsiniz.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ForumFragment myFragment = (ForumFragment) getSupportFragmentManager().findFragmentByTag("fragment_forum");
                            if (myFragment != null && myFragment.isVisible()) {
                                android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                                fragTransaction.detach(myFragment);
                                fragTransaction.attach(myFragment);
                                fragTransaction.commit();
                            }
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NavigationActivity.this);
                    alertDialogBuilder.setTitle("Mesajın Gönderilmesi");
                    alertDialogBuilder.setMessage("Mesajınız şuanda gönderilemiyor.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                }
            }
        }
    }
}
