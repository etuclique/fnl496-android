package com.example.user.fnl496_android;

/**
 * Created by İlayda Şahiner on 22.02.2018.
 */

public class TextNotes {

    int soru_id;
    int sayfaNo;
    String notIcerik;

    public int getSayfaNo() {
        return sayfaNo;
    }

    public void setSayfaNo(int sayfaNo) {
        this.sayfaNo = sayfaNo;
    }

    public String getNotIcerik() {
        return notIcerik;
    }

    public void setNotIcerik(String notIcerik) {
        this.notIcerik = notIcerik;
    }

    public int getSoru_id() {
        return soru_id;
    }

    public void setSoru_id(int soru_id) {
        this.soru_id = soru_id;
    }
}
