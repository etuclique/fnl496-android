package com.example.user.fnl496_android;

/**
 * Created by İlayda Şahiner on 16.03.2018.
 */

public class Ogretmen {
    private int ogretmen_id;
    private String email;
    private String kullaniciAdi;

    public int getOgretmen_id() {
        return ogretmen_id;
    }

    public void setOgretmen_id(int ogretmen_id) {
        this.ogretmen_id = ogretmen_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKullaniciAdi() {
        return kullaniciAdi;
    }

    public void setKullaniciAdi(String kullaniciAdi) {
        this.kullaniciAdi = kullaniciAdi;
    }
}
