package com.example.user.fnl496_android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import adapter.KitaplarCustomAdapter;

/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class KitaplarFragment extends ListFragment {

    ArrayList<Kitap> kitaplar;
    Ogrenci ogrenci;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        kitaplar = new ArrayList<Kitap>();
        return inflater.inflate(R.layout.kitaplar_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Gson gson = new Gson();
        SharedPreferences prefs2 = getActivity().getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs2.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());


        SharedPreferences prefs = getActivity().getSharedPreferences("Kitaplar", Context.MODE_PRIVATE);
        String kitapBilgi = prefs.getString("kitaplar", "isim bulunamadi");
        if(kitapBilgi.equals("isim bulunamadi")){
            new GetOgrenciKitaplariList().execute();
         }else{
            kitaplar = gson.fromJson(kitapBilgi, new TypeToken<ArrayList<Kitap>>() {}.getType());
            KitaplarCustomAdapter adapter = new KitaplarCustomAdapter(getActivity(), kitaplar);
            setListAdapter(adapter);
         }

    }

    private class GetOgrenciKitaplariList extends AsyncTask<String, Void, String> {
        ArrayList<Kitap> kitaplar;
        ProgressDialog progressDialog;
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage("Kitap İçeriği hazırlanıyor...");
            progressDialog.show();

        }
        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/ogrencininkitaplari");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa",jsonStr);

           kitaplar = ParseJsonForKitaplar(jsonStr);
       //    kitaplar= ParseJsonForBook(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            progressDialog.dismiss();
            if (kitaplar == null) {
                Log.i("bosmu", "Issuelar arrayi boş");
            } else {
                KitaplarCustomAdapter adapter = new KitaplarCustomAdapter(getActivity(), kitaplar);
                setListAdapter(adapter);
            }
        }
    }

    private ArrayList<Kitap> ParseJsonForKitaplar(String json) {
        //ArrayList<Issue> issuelar = new ArrayList<Issue>();
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject kitapObj = jsonObj.getJSONObject(i);
                    Kitap kitap = new Kitap();
                    kitap.setKitap_id(kitapObj.getInt("id"));
                    kitap.setKitap_adi(kitapObj.getString("kitapAdi"));
                    kitap.setKitap_yazari(kitapObj.getString("kitapYazari"));
                    kitap.setKitapSayfaNo(kitapObj.getInt("kitapSayfaNo"));
                   /* Bitmap image=null;
                    try {
                        InputStream in = new URL(kitapObj.getString("kitapKapak")).openStream();
                        image= BitmapFactory.decodeStream(in);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    kitap.setKapak_resim(image);*/
                    kitap.setKitap_özet(kitapObj.getString("kitapHtmlText"));
                    kitaplar.add(kitap);

                }

                SharedPreferences preferences2 = getActivity().getSharedPreferences("Kitaplar", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor2 = preferences2.edit();
                Gson gson = new Gson();
                String a = gson.toJson(kitaplar);
                editor2.putString("kitaplar", a);
                editor2.apply();

                SharedPreferences preferences = getActivity().getSharedPreferences("KitapSayisi", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                int kitapSayisi = preferences.getInt("kitapsayisi", 0);
                if(kitapSayisi==0) {
                    editor.putInt("kitapsayisi", kitaplar.size());
                    editor.apply();
                }

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return kitaplar;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(getActivity(), Main2Activity.class);
        intent.putExtra("position_of_clicked_book",position);
        intent.putExtra("id_of_clicked_book",kitaplar.get(position).getKitap_id());
        intent.putExtra("name_of_clicked_book",kitaplar.get(position).getKitap_adi());
        startActivity(intent);
    }
}
