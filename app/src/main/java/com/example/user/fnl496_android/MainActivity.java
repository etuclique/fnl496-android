package com.example.user.fnl496_android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;


public class MainActivity extends AppCompatActivity {


    private static String urlLogin = "https://teacherclique.herokuapp.com/api/students/login";
    private String paramsForLogin ="";
    ProgressDialog progressDialog;
    Ogrenci ogrenci;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button login = (Button) findViewById(R.id.login);
        final EditText password = (EditText) findViewById(R.id.password);
        final EditText email = (EditText) findViewById(R.id.email);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailText = email.getText().toString();
                String passwordText = password.getText().toString();

                Log.v("password:",passwordText);
                Log.v("email: ",emailText);

                if(emailText.length() == 0 ){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("Email Alanı boş!");
                    alertDialogBuilder.setMessage("Lütfen email adresinizi giriniz.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                }else if(!emailText.contains("@")){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("Email Formatınız Yanlış!");
                    alertDialogBuilder.setMessage("Lütfen geçerli bir email adresi yazınız.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                }else if(passwordText.length()==0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("Şifre Alanı boş!");
                    alertDialogBuilder.setMessage("Lütfen Şifrenizi giriniz.");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                        }
                    });
                    alertDialog.show();
                }else{
                    paramsForLogin = "email=" + email.getText().toString().trim() + "&" + "password=" + password.getText().toString().trim();
                    new Login().execute();
                }
            }
        });
    }

    private class Login extends AsyncTask<String, Void, String> {

        String jsonStr = null;
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Signing in...");
            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            try {
                jsonStr = webreq.makeWebServiceCallPost(urlLogin,paramsForLogin);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("bbbbb", jsonStr);

            return "a";
        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            Log.v("bbbbb", jsonStr);
            if(jsonStr.equals("")){
                progressDialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setTitle("Giriş Başarısız");
                alertDialogBuilder.setMessage("Lütfen Kullanıcı Adınızın veya Şifrenizin doğruluğunu kontrol edin.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            }else{
                ParseJsonForOgrenci(jsonStr);
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                new OgretmenBilgileri().execute();
                // Dismiss the progress dialog
            }

        }
    }

    private void ParseJsonForOgrenci(String json) {
        if (json != null) {
            try {
                JSONObject jObj = new JSONObject(json);
                ogrenci = new Ogrenci();
                ogrenci.setOgrenci_id(jObj.getInt("id"));
                ogrenci.setKullaniciAdi(jObj.getString("kullaniciAdi"));
                //ogrenci.getOgrenciFoto(jObj.getInt("ogrenciFoto");
                ogrenci.setSube_id(jObj.getInt("sube_id"));
                ogrenci.setEmail(jObj.getString("email"));

                SharedPreferences preferences = getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                Gson gson = new Gson();
                String a = gson.toJson(ogrenci);
                editor.putString("ogrenci", a);
                editor.apply();

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
    }


    private class OgretmenBilgileri extends AsyncTask<String, Void, String> {


        String jsonStr = "";
        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            try {
                jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/ogrencininogretmeni");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            return "a";
        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if(!jsonStr.equals("{}")){
                ParseJsonForOgretmen(jsonStr);
                Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void ParseJsonForOgretmen(String json) {
        if (json != null) {
            try {
                JSONObject jObj = new JSONObject(json);
                Ogretmen ogretmen = new Ogretmen();
                ogretmen.setOgretmen_id(jObj.getInt("id"));
                ogretmen.setKullaniciAdi(jObj.getString("kullaniciAdi"));
                ogretmen.setEmail(jObj.getString("email"));

                SharedPreferences preferences = getSharedPreferences("OgretmenData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                Gson gson = new Gson();
                String a = gson.toJson(ogretmen);
                editor.putString("ogretmen", a);
                editor.apply();

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
    }


}
