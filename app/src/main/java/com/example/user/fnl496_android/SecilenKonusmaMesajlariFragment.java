package com.example.user.fnl496_android;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapter.KelimelerCustomAdapter;
import adapter.MessagesCustomAdapter;

/**
 * Created by İlayda Şahiner on 09.03.2018.
 */

public class SecilenKonusmaMesajlariFragment extends ListFragment {

        int conversation_id;
        ArrayList<Message> mesajlar;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = this.getArguments();
            if (bundle != null)
            {
                conversation_id = bundle.getInt("id_of_clicked_conversation", 0);
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            mesajlar = new ArrayList<Message>();
            View view = inflater.inflate(R.layout.messages_fragment, container, false);
            view.setBackgroundResource(R.color.colorWhite);
            return view;

        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            new GetKelimelerList().execute();

        }

        private class GetKelimelerList extends AsyncTask<String, Void, String> {
            ArrayList<Message> mesajlar;

            @Override
            protected String doInBackground(String... arg0) {

                WebRequest webreq = new WebRequest();
                String jsonStr = null;
                try {
                    jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/conversations/"+conversation_id+"/messages");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.v("aaaaa", jsonStr);

                mesajlar = ParseJsonForMesajlar(jsonStr);
                //    kitaplar= ParseJsonForBook(jsonStr);
                return "a";

            }


            @Override
            protected void onPostExecute(String requestresult) {
                super.onPostExecute(requestresult);
                if (mesajlar == null) {
                    Log.i("bosmu", "Issuelar arrayi boş");
                } else {
                    MessagesCustomAdapter adapter = new MessagesCustomAdapter(getActivity(), mesajlar);
                    setListAdapter(adapter);

                }
            }


        }

        private ArrayList<Message> ParseJsonForMesajlar(String json) {
            if (json != null) {
                try {
                    JSONArray jsonObj = new JSONArray(json);
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject mesajObj = jsonObj.getJSONObject(i);
                        Message mesaj = new Message();
                        mesaj.setMessage_id(mesajObj.getInt("id"));
                        mesaj.setConversation_id(mesajObj.getInt("conversation_id"));
                        mesaj.setBody(mesajObj.getString("body"));
                        mesaj.setSender(mesajObj.getBoolean("sender"));

                        mesajlar.add(mesaj);

                    }
                } catch (JSONException e) {
                    Log.e("MYAPP", "unexpected JSON exception", e);
                    // Do something to recover ... or kill the app.
                }
            }
            return mesajlar;
        }


}


