package com.example.user.fnl496_android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapter.CevaplanmisDogruYanlisCustomAdapter;
import adapter.DogruYanlisCustomAdapter;
import objects.cevaplanmisDySorular;
import objects.dogruYanlisSoru;

/**
 * Created by feyzaiyi on 25.01.2018.
 */

public class SorularDogruYanlis extends ListFragment{

    int kitap_id;

    Ogrenci ogrenci;
    View p;

    ArrayList<cevaplanmisDySorular> cevaplanmisDySorular;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        Bundle bundle = this.getArguments();
        if (bundle != null)
        {
            kitap_id = bundle.getInt("id_of_clicked_book5", 0);
            Log.v("yy",String.valueOf(kitap_id));

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        cevaplanmisDySorular = new ArrayList<cevaplanmisDySorular>();
        return inflater.inflate(R.layout.sorular_dogru_yanlis,container,false);


    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Gson gson = new Gson();
        SharedPreferences prefs = getActivity().getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

        new GetCevaplanmisDySorular().execute();

    }


    private class GetCevaplanmisDySorular extends AsyncTask<String, Void, String> {
        ArrayList<cevaplanmisDySorular> cevaplanmisDySorular;
        String urlDogruYanlis = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+ kitap_id +"/ogrencinincevaplari";

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();

            String jsonStr = null;

            try {
                    jsonStr = webreq.makeWebServiceCallGet(urlDogruYanlis);

            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa",jsonStr);

            cevaplanmisDySorular = ParseJsonForCevaplanmisDySorular(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (cevaplanmisDySorular == null) {
                Log.i("bosmu", "Issuelar arrayi boş");
            } else {
                CevaplanmisDogruYanlisCustomAdapter adapter = new CevaplanmisDogruYanlisCustomAdapter(getActivity(), cevaplanmisDySorular);
                setListAdapter(adapter);
            }
        }
    }


   private ArrayList<cevaplanmisDySorular> ParseJsonForCevaplanmisDySorular(String json) {

        if (json != null) {
            try {
                JSONObject jsonObj = new JSONObject(json);
                JSONArray dySorularArray = jsonObj.getJSONArray("dogruYanlisSoruBilgileri");
                JSONArray dyOgrenciCevaplariArray = jsonObj.getJSONArray("studentDogruYanlisSoruCevaplari");

                for (int i = 0; i < dySorularArray.length(); i++) {

                    JSONObject soruObj = dySorularArray.getJSONObject(i);

                    cevaplanmisDySorular soru = new cevaplanmisDySorular();
                    soru.setSoru_id(soruObj.getInt("id"));
                    soru.setBook_id(soruObj.getInt("book_id"));
                    soru.setSoruText(soruObj.getString("soruText"));
                    soru.setDogruCevap(soruObj.getString("dogruCevap"));

                    JSONObject soruObj1 = dyOgrenciCevaplariArray.getJSONObject(i);
                    soru.setOgrenciCevap(soruObj1.getString("cevap"));
                    soru.setStudent_id(soruObj1.getInt("student_id"));

                    cevaplanmisDySorular.add(soru);
                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return cevaplanmisDySorular;
    }


}
