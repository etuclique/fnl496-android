package com.example.user.fnl496_android;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.StringBuilderPrinter;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class SkorActivity extends AppCompatActivity {
    SharedPreferences defaults;

    PieChart pieChart;
    ArrayList<Entry> entries;
    ArrayList<String> PieEntryLabels;
    PieDataSet pieDataSet;
    PieData pieData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skor);
        defaults = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String kitapCevaplar = "kitapCevaplar";
        String kitapVerilenCevaplar = "kitapVerilenCevaplar";

        String getirKitapCevaplar;
        String getirKitapVerilenCevaplar;

        String skor = ""; //dogru-yanlis-bos bosluk
        boolean getir = true;
        int i = 0;

        while (getir) {
            i++;
            kitapCevaplar = kitapCevaplar + String.valueOf(i);
            kitapVerilenCevaplar = kitapVerilenCevaplar + String.valueOf(i);
            getirKitapCevaplar = defaults.getString(kitapCevaplar, "");
            getirKitapVerilenCevaplar = defaults.getString(kitapVerilenCevaplar, "");

            String[] cevap = getirKitapCevaplar.split("-");
            String[] verilenCevap = getirKitapVerilenCevaplar.split("-");

            int dogru = 0;
            int yanlis = 0;
            int bos = 0;

            for (int j = 0; j < cevap.length; j++) {
                if (verilenCevap.equals("")) {
                    bos++;
                } else if (verilenCevap[j].equals(cevap[j])) {
                    dogru++;
                } else {
                    yanlis++;
                }
            }
            setSkorChart(dogru,yanlis,bos);

            if (getirKitapCevaplar.equals("") || getirKitapVerilenCevaplar.equals(""))
                getir = false;
        }

    }

    public void setSkorChart(int d, int y, int b){

        pieChart = (PieChart) findViewById(R.id.chart1);
        entries = new ArrayList<>();
        PieEntryLabels = new ArrayList<String>();
        AddValuesToPIEENTRY(d,y,b);
        AddValuesToPieEntryLabels();
        pieDataSet = new PieDataSet(entries, "");
        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieChart.setData(pieData);
        pieChart.animateY(3000);
    }

    public void AddValuesToPIEENTRY(int d, int y, int b) {

        entries.add(new BarEntry(d, 0));
        entries.add(new BarEntry(y, 1));
        entries.add(new BarEntry(b, 2));


    }

    public void AddValuesToPieEntryLabels() {

        PieEntryLabels.add("Doğru");
        PieEntryLabels.add("Yanlış");
        PieEntryLabels.add("Boş");

    }

}
