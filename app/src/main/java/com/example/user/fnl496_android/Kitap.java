package com.example.user.fnl496_android;

import android.graphics.Bitmap;

/**
 * Created by İlayda Şahiner on 15.01.2018.
 */

public class Kitap {

    int kitap_id;
    String kitap_adi;
    String kitap_yazari;
    int kitapSayfaNo;
    String HtmlIcerik;
    Bitmap kapak_resim;
    String kitap_özet;


    public int getKitap_id() {
        return kitap_id;
    }

    public void setKitap_id(int kitap_id) {
        this.kitap_id = kitap_id;
    }

    public String getKitap_adi() {
        return kitap_adi;
    }

    public void setKitap_adi(String kitap_adi) {
        this.kitap_adi = kitap_adi;
    }

    public String getKitap_yazari() {
        return kitap_yazari;
    }

    public void setKitap_yazari(String kitap_yazari) {
        this.kitap_yazari = kitap_yazari;
    }

    public int getKitapSayfaNo() {
        return kitapSayfaNo;
    }

    public void setKitapSayfaNo(int kitapSayfaNo) {
        this.kitapSayfaNo = kitapSayfaNo;
    }

    public String getHtmlIcerik() {
        return HtmlIcerik;
    }

    public void setHtmlIcerik(String htmlIcerik) {
        HtmlIcerik = htmlIcerik;
    }

    public Bitmap getKapak_resim() {
        return kapak_resim;
    }

    public void setKapak_resim(Bitmap kapak_resim) {
        this.kapak_resim = kapak_resim;
    }

    public String getKitap_özet() {
        return kitap_özet;
    }

    public void setKitap_özet(String kitap_özet) {
        this.kitap_özet = kitap_özet;
    }
}
