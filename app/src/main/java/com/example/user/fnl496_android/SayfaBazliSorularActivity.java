package com.example.user.fnl496_android;

import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import adapter.DogruYanlisCustomAdapter;
import adapter.EslemeCustomAdapter;
import adapter.KelimelerCustomAdapter;
import adapter.SecmeCustomAdapter;
import adapter.TextNoteCustomAdapter;
import objects.EslemeSoru;
import objects.SecmeliSoru;
import objects.dogruYanlisSoru;

public class SayfaBazliSorularActivity extends ListActivity{

    ArrayList<dogruYanlisSoru> sorular1;
    ArrayList<EslemeSoru> sorular2;
    ArrayList<SecmeliSoru> sorular3;
    int kitap_id;
    int sayfaNo;
    Ogrenci ogrenci;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sayfa_bazli_sorular);

        Gson gson = new Gson();
        SharedPreferences prefs = getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

        sorular1 = new ArrayList<dogruYanlisSoru>();
        sorular2 = new ArrayList<EslemeSoru>();
        sorular3 = new ArrayList<SecmeliSoru>();

        kitap_id = getIntent().getExtras().getInt("id_of_clicked_book", 0);
        sayfaNo=getIntent().getExtras().getInt("current_page_no", 0);

        new GetKitaptakiSayfaBazliSecSorularList().execute();

        Button sec1 = (Button) findViewById(R.id.sec1);
        Button sec2 = (Button) findViewById(R.id.sec2);
        Button sec3 = (Button) findViewById(R.id.sec3);


        sec1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sorular1.clear();
                    new GetKitaptakiSayfaBazliDySorularList().execute();

                }
            });
        sec2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sorular2.clear();
                    new GetKitaptakiSayfaBazliEsSorularList().execute();
                }
            });
        sec3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sorular3.clear();
                    new GetKitaptakiSayfaBazliSecSorularList().execute();
                }
            });

    }

    private class GetKitaptakiSayfaBazliDySorularList extends AsyncTask<String, Void, String> {


        String url = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+ kitap_id +"/sayfa_dys/" + sayfaNo;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = "";
            try {
                jsonStr = webreq.makeWebServiceCallGet(url);

            }
            catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            sorular1.clear();
            sorular1 = ParseJsonForDySorular(jsonStr);

            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            Log.v("secsize1",""+sorular1.size());
            DogruYanlisCustomAdapter adapter = new DogruYanlisCustomAdapter(SayfaBazliSorularActivity.this, sorular1);
            setListAdapter(adapter);
        }

    }

    private ArrayList<dogruYanlisSoru> ParseJsonForDySorular(String json) {
        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject dyObj = jsonObj.getJSONObject(i);
                    dogruYanlisSoru dy = new dogruYanlisSoru();
                    dy.setSoruId(dyObj.getInt("id"));
                    dy.setSoruText(dyObj.getString("soruText"));
                    dy.setDogruCevap(dyObj.getString("dogruCevap"));
                    WebRequest webreq = new WebRequest();
                    String jsonStr = "";
                    try {
                        jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/students_dyquestions/" + dy.getSoruId());
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(jsonStr.equals("") ||jsonStr.equals(" ") || jsonStr.contains("[]") ){
                        //true
                        sorular1.add(dy);
                    }

                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return sorular1;
    }


    private class GetKitaptakiSayfaBazliEsSorularList extends AsyncTask<String, Void, String> {


        String url = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+ kitap_id +"/sayfa_ess/" + sayfaNo;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = "";
            try {
                jsonStr = webreq.makeWebServiceCallGet(url);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            sorular2.clear();
            sorular2 = ParseJsonForEslemeSorular(jsonStr);

            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            Log.v("secsize2",""+sorular2.size());
            EslemeCustomAdapter adapter = new EslemeCustomAdapter(SayfaBazliSorularActivity.this, sorular2);
            setListAdapter(adapter);
        }

    }

    private ArrayList<EslemeSoru> ParseJsonForEslemeSorular(String json) {

        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject esObj = jsonObj.getJSONObject(i);
                    EslemeSoru es = new EslemeSoru();
                    es.setSoruId(esObj.getInt("id"));
                    es.setSoruText(esObj.getString("soruText"));
                    es.setYandakiText(esObj.getString("yandakiText"));
                    es.setDogruHarf(esObj.getString("dogruHarf"));
                    WebRequest webreq = new WebRequest();
                    String jsonStr = "";
                    try {
                        jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/students_esquestions/" + es.getSoruId());

                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("aaaaa", jsonStr);

                    if(jsonStr.equals("") ||jsonStr.equals(" ") || jsonStr.contains("[]") ){
                        //true
                        sorular2.add(es);
                    }

                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return sorular2;
    }


    private class GetKitaptakiSayfaBazliSecSorularList extends AsyncTask<String, Void, String> {


        String url = "https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/books/"+ kitap_id +"/sayfa_secs/" + sayfaNo;

        @Override
        protected String doInBackground(String... arg0) {

            WebRequest webreq = new WebRequest();
            String jsonStr = "";

            try {
                jsonStr = webreq.makeWebServiceCallGet(url);

            }catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", jsonStr);
            sorular3.clear();
            sorular3 = ParseJsonForSecmeSorular(jsonStr);

            return "a";

        }

        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            Log.v("secsize3",""+sorular3.size());
            SecmeCustomAdapter adapter = new SecmeCustomAdapter(SayfaBazliSorularActivity.this, sorular3);
            setListAdapter(adapter);
        }
    }

    private ArrayList<SecmeliSoru> ParseJsonForSecmeSorular(String json) {

        if (json != null) {
            try {
                JSONArray jsonObj = new JSONArray(json);
                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject sObj = jsonObj.getJSONObject(i);
                    SecmeliSoru s = new SecmeliSoru();
                    s.setSoruId(sObj.getInt("id"));
                    s.setSoruText(sObj.getString("soruText"));
                    s.setaText(sObj.getString("aText"));
                    s.setbText(sObj.getString("bText"));
                    s.setcText(sObj.getString("cText"));
                    s.setdText(sObj.getString("dText"));
                    s.setDogruCevap(sObj.getString("dogruCevap"));

                    WebRequest webreq = new WebRequest();
                    String jsonStr = "";
                    try {
                        jsonStr = webreq.makeWebServiceCallGet("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/students_secquestions/" +s.getSoruId() );

                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v("aaaaa", jsonStr);

                    if(jsonStr.equals("") ||jsonStr.equals(" ") || jsonStr.contains("[]") ){
                        //true
                        sorular3.add(s);
                    }

                }
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        return sorular3;
    }
}
