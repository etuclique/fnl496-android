package com.example.user.fnl496_android;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapter.KitaplarCustomAdapter;

/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class SecilenKitabinSorulariFragment extends ListFragment {


        private static String urlKitaplar = "https://teacherclique.herokuapp.com/api/books/";
        ArrayList<Kitap> kitaplar;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            kitaplar = new ArrayList<Kitap>();
            return inflater.inflate(R.layout.kitaplar_fragment,container,false);
        }

        @Override
        public void onViewCreated(View view,Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            Gson gson = new Gson();
            SharedPreferences prefs = getActivity().getSharedPreferences("Kitaplar", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();

            String kitapBilgi = prefs.getString("kitaplar", "isim bulunamadi");
            if(kitapBilgi.equals("isim bulunamadi")){
                new GetOgrenciKitaplariList().execute();
            }else{
                kitaplar = gson.fromJson(kitapBilgi, new TypeToken<ArrayList<Kitap>>() {}.getType());
                KitaplarCustomAdapter adapter = new KitaplarCustomAdapter(getActivity(), kitaplar);
                setListAdapter(adapter);
            }

        }

        private class GetOgrenciKitaplariList extends AsyncTask<String, Void, String> {
            ArrayList<Kitap> kitaplar;

            @Override
            protected String doInBackground(String... arg0) {

                WebRequest webreq = new WebRequest();
                String jsonStr = null;
                try {
                    jsonStr = webreq.makeWebServiceCallGet(urlKitaplar);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.v("aaaaa",jsonStr);

                kitaplar = ParseJsonForKitaplar(jsonStr);
                //    kitaplar= ParseJsonForBook(jsonStr);
                return "a";

            }


            @Override
            protected void onPostExecute(String requestresult) {
                super.onPostExecute(requestresult);
                if (kitaplar == null) {
                    Log.i("bosmu", "Issuelar arrayi boş");
                } else {
                    KitaplarCustomAdapter adapter = new KitaplarCustomAdapter(getActivity(), kitaplar);
                    setListAdapter(adapter);
                }
            }
        }

        private ArrayList<Kitap> ParseJsonForKitaplar(String json) {
            //ArrayList<Issue> issuelar = new ArrayList<Issue>();
            if (json != null) {
                try {
                    JSONArray jsonObj = new JSONArray(json);
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject kitapObj = jsonObj.getJSONObject(i);
                        Kitap kitap = new Kitap();
                        kitap.setKitap_id(kitapObj.getInt("id"));
                        kitap.setKitap_adi(kitapObj.getString("kitapAdi"));
                        kitap.setKitap_yazari(kitapObj.getString("kitapYazari"));
                        kitap.setKitapSayfaNo(kitapObj.getInt("kitapSayfaNo"));
                        kitap.setHtmlIcerik(kitapObj.getString("kitapHtmlIcerik"));
                        // kitap.setKapak_resim(kitapObj.getString(kitapKapak));
                        /*issue.setIsItDone(issueObj.getBoolean("isItDone"));
                        issue.setOverDue(issueObj.getBoolean("overDue"));*/

                        kitaplar.add(kitap);
                    }

                    SharedPreferences preferences = getActivity().getSharedPreferences("Kitaplar", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    Gson gson = new Gson();
                    String a = gson.toJson(kitaplar);
                    editor.putString("kitaplar", a);
                    editor.apply();

                } catch (JSONException e) {
                    Log.e("MYAPP", "unexpected JSON exception", e);
                    // Do something to recover ... or kill the app.
                }
            }
            return kitaplar;
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);

            SınavlarFragment fragment_secilen= new SınavlarFragment();
            Bundle bundle = new Bundle();
            Log.v("ddd",""+kitaplar.get(position).getKitap_id());
            bundle.putInt("id_of_clicked_book2",kitaplar.get(position).getKitap_id());
            fragment_secilen.setArguments(bundle);
            FragmentTransaction trans = getFragmentManager().beginTransaction();
            trans.replace(R.id.f,fragment_secilen,"fragment_secilen");
            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            trans.addToBackStack(null);
            trans.commit();
        }



    }

