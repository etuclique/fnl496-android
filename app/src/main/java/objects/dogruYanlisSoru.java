package objects;

/**
 * Created by feyzaiyi on 25.01.2018.
 */

public class dogruYanlisSoru {

    private String soruText;
    private String dogruCevap;
    private int kitapId;
    private int sayfaNo;
    private int soruId;

    public String getSoruText() {
        return soruText;
    }

    public void setSoruText(String soruText) {
        this.soruText = soruText;
    }

    public String getDogruCevap() {
        return dogruCevap;
    }

    public void setDogruCevap(String dogruCevap) {
        this.dogruCevap = dogruCevap;
    }

    public int getKitapId() {
        return kitapId;
    }

    public void setKitapId(int kitapId) {
        this.kitapId = kitapId;
    }

    public int getSayfaNo() {
        return sayfaNo;
    }

    public void setSayfaNo(int sayfaNo) {
        this.sayfaNo = sayfaNo;
    }

    public int getSoruId() {
        return soruId;
    }

    public void setSoruId(int soruId) {
        this.soruId = soruId;
    }
}
