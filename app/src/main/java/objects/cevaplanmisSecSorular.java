package objects;

/**
 * Created by feyzaiyi on 24.03.2018.
 */

public class cevaplanmisSecSorular {
    int student_id;
    int book_id;
    int soru_id;
    String soruText;
    String aText;
    String bText;
    String cText;
    String dText;
    String dogruCevap;
    String ogrenciCevap;

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public int getSoru_id() {
        return soru_id;
    }

    public void setSoru_id(int soru_id) {
        this.soru_id = soru_id;
    }

    public String getSoruText() {
        return soruText;
    }

    public void setSoruText(String soruText) {
        this.soruText = soruText;
    }

    public String getaText() {
        return aText;
    }

    public void setaText(String aText) {
        this.aText = aText;
    }

    public String getbText() {
        return bText;
    }

    public void setbText(String bText) {
        this.bText = bText;
    }

    public String getcText() {
        return cText;
    }

    public void setcText(String cText) {
        this.cText = cText;
    }

    public String getdText() {
        return dText;
    }

    public void setdText(String dText) {
        this.dText = dText;
    }

    public String getDogruCevap() {
        return dogruCevap;
    }

    public void setDogruCevap(String dogruCevap) {
        this.dogruCevap = dogruCevap;
    }

    public String getOgrenciCevap() {
        return ogrenciCevap;
    }

    public void setOgrenciCevap(String ogrenciCevap) {
        this.ogrenciCevap = ogrenciCevap;
    }
}
