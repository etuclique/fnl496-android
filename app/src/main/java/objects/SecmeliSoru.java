package objects;

/**
 * Created by feyzaiyi on 26.01.2018.
 */

public class SecmeliSoru {
    private String soruText;
    private  String aText;
    private  String bText;
    private  String cText;
    private  String dText;
    private  String dogruCevap;
    private int kitapId;
    private int sayfaNo;
    private int soruId;


    public String getSoruText() {
        return soruText;
    }

    public void setSoruText(String soruText) {
        this.soruText = soruText;
    }

    public String getaText() {
        return aText;
    }

    public void setaText(String aText) {
        this.aText = aText;
    }

    public String getbText() {
        return bText;
    }

    public void setbText(String bText) {
        this.bText = bText;
    }

    public String getcText() {
        return cText;
    }

    public void setcText(String cText) {
        this.cText = cText;
    }

    public String getdText() {
        return dText;
    }

    public void setdText(String dText) {
        this.dText = dText;
    }

    public String getDogruCevap() {
        return dogruCevap;
    }

    public void setDogruCevap(String dogruCevap) {
        this.dogruCevap = dogruCevap;
    }

    public int getKitapId() {
        return kitapId;
    }

    public void setKitapId(int kitapId) {
        this.kitapId = kitapId;
    }

    public int getSayfaNo() {
        return sayfaNo;
    }

    public void setSayfaNo(int sayfaNo) {
        this.sayfaNo = sayfaNo;
    }

    public int getSoruId() {
        return soruId;
    }

    public void setSoruId(int soruId) {
        this.soruId = soruId;
    }
}
