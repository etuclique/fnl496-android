package objects;

/**
 * Created by feyzaiyi on 26.01.2018.
 */

public class EslemeSoru {
    private String soruText;
    private  String yandakiText;
    private String dogruHarf;
    private int kitapId;
    private int sayfaNo;
    private int soruId;

    public String getSoruText() {
        return soruText;
    }

    public void setSoruText(String soruText) {
        this.soruText = soruText;
    }

    public String getYandakiText() {
        return yandakiText;
    }

    public void setYandakiText(String yandakiText) {
        this.yandakiText = yandakiText;
    }

    public String getDogruHarf() {
        return dogruHarf;
    }

    public void setDogruHarf(String dogruHarf) {
        this.dogruHarf = dogruHarf;
    }

    public int getKitapId() {
        return kitapId;
    }

    public void setKitapId(int kitapId) {
        this.kitapId = kitapId;
    }

    public int getSayfaNo() {
        return sayfaNo;
    }

    public void setSayfaNo(int sayfaNo) {
        this.sayfaNo = sayfaNo;
    }

    public int getSoruId() {
        return soruId;
    }

    public void setSoruId(int soruId) {
        this.soruId = soruId;
    }
}
