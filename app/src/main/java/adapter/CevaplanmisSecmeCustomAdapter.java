package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.fnl496_android.Ogrenci;
import com.example.user.fnl496_android.R;
import com.example.user.fnl496_android.WebRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;

import objects.SecmeliSoru;
import objects.cevaplanmisSecSorular;


/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class CevaplanmisSecmeCustomAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<cevaplanmisSecSorular> cevaplanmisSecSorular;
    Ogrenci ogrenci;
    cevaplanmisSecSorular cevaplanmisSecSoru;


    public CevaplanmisSecmeCustomAdapter(Activity activity, ArrayList<cevaplanmisSecSorular> list) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        cevaplanmisSecSorular = list;

        Gson gson = new Gson();
        SharedPreferences prefs = activity.getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

    }

    @Override
    public int getCount() {
        return cevaplanmisSecSorular.size();
    }

    @Override
    public Object getItem(int position) {
        return cevaplanmisSecSorular.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_secme, null);

        final Button soruAButton = (Button) satirView.findViewById(R.id.soruAButton);
        final Button soruBButton = (Button) satirView.findViewById(R.id.soruBButton);
        final Button soruCButton = (Button) satirView.findViewById(R.id.soruCButton);
        final Button soruDButton = (Button) satirView.findViewById(R.id.soruDButton);
        final Button onay = (Button) satirView.findViewById(R.id.onay);
        final ImageView checkdogru = (ImageView) satirView.findViewById(R.id.checkdogru);
        final ImageView checkyanlis = (ImageView) satirView.findViewById(R.id.checkyanlis);

        TextView soruAText = (TextView) satirView.findViewById(R.id.soruAText);
        TextView soruBText = (TextView) satirView.findViewById(R.id.soruBText);
        TextView soruCText = (TextView) satirView.findViewById(R.id.soruCText);
        TextView soruDText = (TextView) satirView.findViewById(R.id.soruDText);
        TextView soruTextSecme = (TextView) satirView.findViewById(R.id.soruTextSecme);
        TextView secmisOldugunuzText = (TextView) satirView.findViewById(R.id.secmisOldugunuzText);
        final TextView secilen = (TextView) satirView.findViewById(R.id.secilen);

        LinearLayout secmeShow = (LinearLayout) satirView.findViewById(R.id.secmeShow);
        TextView dogruCevapTextAl = (TextView) satirView.findViewById(R.id.dogruCevapTextAl);
        TextView verilenCevapTextAl = (TextView) satirView.findViewById(R.id.verilenCevapTextAl);
        checkdogru.setVisibility(View.GONE);
        checkyanlis.setVisibility(View.GONE);

        cevaplanmisSecSoru = cevaplanmisSecSorular.get(position);


            soruBButton.setClickable(false);
            soruCButton.setClickable(false);
            soruDButton.setClickable(false);
            soruAButton.setClickable(false);
            soruTextSecme.setVisibility(View.VISIBLE);
            onay.setVisibility(View.GONE);
            secilen.setVisibility(View.GONE);
            secmisOldugunuzText.setVisibility(View.GONE);
            secmeShow.setVisibility(View.VISIBLE);

            dogruCevapTextAl.setText(cevaplanmisSecSoru.getDogruCevap());
            verilenCevapTextAl.setText(cevaplanmisSecSoru.getOgrenciCevap());


        if(cevaplanmisSecSoru.getDogruCevap().equalsIgnoreCase("A")){
            soruAButton.setBackgroundColor(Color.GREEN);
        }else if(cevaplanmisSecSoru.getDogruCevap().equalsIgnoreCase("B")){
            soruBButton.setBackgroundColor(Color.GREEN);
        }else if(cevaplanmisSecSoru.getDogruCevap().equalsIgnoreCase("C")){
            soruCButton.setBackgroundColor(Color.GREEN);
        }else if(cevaplanmisSecSoru.getDogruCevap().equalsIgnoreCase("D")){
            soruDButton.setBackgroundColor(Color.GREEN);
        }

        if(cevaplanmisSecSoru.getDogruCevap().equals(cevaplanmisSecSoru.getOgrenciCevap())){
            checkdogru.setVisibility(View.VISIBLE);
        }else{
            checkyanlis.setVisibility(View.VISIBLE);
            if(cevaplanmisSecSoru.getOgrenciCevap().equalsIgnoreCase("A")){
                soruAButton.setBackgroundColor(Color.RED);
            }else if(cevaplanmisSecSoru.getOgrenciCevap().equalsIgnoreCase("B")){
                soruBButton.setBackgroundColor(Color.RED);
            }else if(cevaplanmisSecSoru.getOgrenciCevap().equalsIgnoreCase("C")){
                soruCButton.setBackgroundColor(Color.RED);
            }else if(cevaplanmisSecSoru.getOgrenciCevap().equalsIgnoreCase("D")){
                soruDButton.setBackgroundColor(Color.RED);
            }
        }


        final TextView dogruCevap = (TextView) satirView.findViewById(R.id.cevap);
        dogruCevap.setVisibility(View.GONE);
        final TextView cevapText = (TextView) satirView.findViewById(R.id.cevaptext);
        cevapText.setVisibility(View.GONE);


        soruTextSecme.setText(String.valueOf(cevaplanmisSecSoru.getSoruText()));
        soruAText.setText(String.valueOf(cevaplanmisSecSoru.getaText()));
        soruBText.setText(String.valueOf(cevaplanmisSecSoru.getbText()));
        soruCText.setText(String.valueOf(cevaplanmisSecSoru.getcText()));
        soruDText.setText(String.valueOf(cevaplanmisSecSoru.getdText()));

        return satirView;
    }

}
