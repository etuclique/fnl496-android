package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.user.fnl496_android.Ogrenci;
import com.example.user.fnl496_android.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import objects.cevaplanmisDySorular;



/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class CevaplanmisDogruYanlisCustomAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<cevaplanmisDySorular> cevaplanmisDySorular;
    Ogrenci ogrenci;
    cevaplanmisDySorular cevaplanmisDySoru;

    public CevaplanmisDogruYanlisCustomAdapter(Activity activity, ArrayList<cevaplanmisDySorular> list) {

        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        cevaplanmisDySorular = list;

        Gson gson = new Gson();
        SharedPreferences prefs = activity.getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());


    }

    @Override
    public int getCount() {
        return cevaplanmisDySorular.size();
    }

    @Override
    public Object getItem(int position) {
        return cevaplanmisDySorular.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_dogru_yanlis, null);


        TextView soruText = (TextView) satirView.findViewById(R.id.soruText);
        final Button trueButton = (Button) satirView.findViewById(R.id.trueButton);
        final Button falseButton = (Button) satirView.findViewById(R.id.falseButton);
        final Button onay = (Button) satirView.findViewById(R.id.dogruyanlisonay);
        final ImageView checkdogru = (ImageView) satirView.findViewById(R.id.dogru);
        final ImageView checkyanlis = (ImageView) satirView.findViewById(R.id.yanlis);
        final TextView dogruCevap = (TextView) satirView.findViewById(R.id.dogruyanliscevap);
        final TextView cevapText = (TextView) satirView.findViewById(R.id.dogruyanliscevaptext);
        LinearLayout dogruYanlisShow = (LinearLayout) satirView.findViewById(R.id.dogruYanlisShow);
        final TextView dogruCevapTextAl = (TextView) satirView.findViewById(R.id.dogruCevapTextAl);
        final TextView verilenCevapTextAl = (TextView) satirView.findViewById(R.id.verilenCevapTextAl);

        cevapText.setVisibility(View.GONE);
        dogruCevap.setVisibility(View.GONE);
        checkdogru.setVisibility(View.GONE);
        checkyanlis.setVisibility(View.GONE);

        cevaplanmisDySoru = cevaplanmisDySorular.get(position);

        onay.setVisibility(View.GONE);
        dogruYanlisShow.setVisibility(View.VISIBLE);
        trueButton.setVisibility(View.GONE);
        falseButton.setVisibility(View.GONE);
        dogruCevapTextAl.setText(cevaplanmisDySoru.getDogruCevap());
        verilenCevapTextAl.setText(cevaplanmisDySoru.getOgrenciCevap());
        soruText.setText(cevaplanmisDySoru.getSoruText());

        if(cevaplanmisDySoru.getDogruCevap().toString().equalsIgnoreCase(cevaplanmisDySoru.getOgrenciCevap().toString())){
            checkdogru.setVisibility(View.VISIBLE);
        }
        else {
            checkyanlis.setVisibility(View.VISIBLE);
        }

        return satirView;
    }


}
