package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.fnl496_android.Ogrenci;
import com.example.user.fnl496_android.R;
import com.example.user.fnl496_android.WebRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;

import objects.EslemeSoru;


/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class EslemeCustomAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<EslemeSoru> eslemeSorular;
    String cevap="";
    Ogrenci ogrenci;
    EslemeSoru eslemeSoru;


    public EslemeCustomAdapter(Activity activity, ArrayList<EslemeSoru> list) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        eslemeSorular = list;

        Gson gson = new Gson();
        SharedPreferences prefs = activity.getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

    }


    @Override
    public int getCount() {
        return eslemeSorular.size();
    }

    @Override
    public Object getItem(int position) {
        return eslemeSorular.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_esleme, null);


        final EditText cevapAl = (EditText) satirView.findViewById(R.id.cevapAl);
        TextView soruEslemeText = (TextView) satirView.findViewById(R.id.soruEslemeText);
        TextView yanindakiText = (TextView) satirView.findViewById(R.id.yanindakiText);
        TextView basindakiHarf = (TextView) satirView.findViewById(R.id.basindakiHarf);

        final Button submit = (Button) satirView.findViewById(R.id.submit);
        final ImageView eslemedogru = (ImageView) satirView.findViewById(R.id.eslemedogru);
        final ImageView eslemeyanlis = (ImageView) satirView.findViewById(R.id.eslemeyanlis);

        LinearLayout eslemeShow = (LinearLayout) satirView.findViewById(R.id.eslemeShow);

        cevapAl.setVisibility(View.VISIBLE);
        eslemeShow.setVisibility(View.GONE);
        submit.setVisibility(View.VISIBLE);
        eslemedogru.setVisibility(View.GONE);
        eslemeyanlis.setVisibility(View.GONE);

        eslemeSoru = eslemeSorular.get(position);


        soruEslemeText.setText(String.valueOf(eslemeSoru.getSoruText()));
        yanindakiText.setText(String.valueOf(eslemeSoru.getYandakiText()));

        if (position%4==0){
            basindakiHarf.setText(String.valueOf("A"));
        }
        else if (position%4==1){
            basindakiHarf.setText(String.valueOf("B"));
        }
        else if (position%4==2){
            basindakiHarf.setText(String.valueOf("C"));
        }  else {
            basindakiHarf.setText(String.valueOf("D"));
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cevap = cevapAl.getText().toString();
                cevapAl.setText(cevap);
                submit.setVisibility(View.GONE);
                if(cevap.equals(eslemeSoru.getDogruHarf())){
                    submit.setVisibility(View.GONE);
                    eslemedogru.setVisibility(View.VISIBLE);
                }else{
                    submit.setVisibility(View.GONE);
                    eslemeyanlis.setVisibility(View.VISIBLE);
                }
                new EslemeliCevabiEkle().execute();
            }
        });


        return satirView;
    }

    private class EslemeliCevabiEkle extends AsyncTask<String, Void, String> {
        int responseCode = 0;

        @Override
        protected String doInBackground(String... params) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                responseCode = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/esleme_questions/"+eslemeSoru.getSoruId()+"/students_esquestions","cevap=" + cevap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("AAA", ""+responseCode);
            return "a";
        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
        }
    }

}
