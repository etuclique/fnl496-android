package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.fnl496_android.Kitap;
import com.example.user.fnl496_android.Ogrenci;
import com.example.user.fnl496_android.R;
import com.example.user.fnl496_android.WebRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;

import objects.dogruYanlisSoru;


/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class DogruYanlisCustomAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<dogruYanlisSoru> dogruYanlisSorular;
    String cevap="";
    Ogrenci ogrenci;
    dogruYanlisSoru dogruYanlisSoru;


    public DogruYanlisCustomAdapter(Activity activity, ArrayList<dogruYanlisSoru> list) {

        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        dogruYanlisSorular = list;

        Gson gson = new Gson();
        SharedPreferences prefs = activity.getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

    }

    @Override
    public int getCount() {
        return dogruYanlisSorular.size();
    }

    @Override
    public Object getItem(int position) {
        return dogruYanlisSorular.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_dogru_yanlis, null);


        TextView soruText = (TextView) satirView.findViewById(R.id.soruText);
        final Button trueButton = (Button) satirView.findViewById(R.id.trueButton);
        final Button falseButton = (Button) satirView.findViewById(R.id.falseButton);
        final Button onay = (Button) satirView.findViewById(R.id.dogruyanlisonay);
        final ImageView checkdogru = (ImageView) satirView.findViewById(R.id.dogru);
        final ImageView checkyanlis = (ImageView) satirView.findViewById(R.id.yanlis);
        final TextView dogruCevap = (TextView) satirView.findViewById(R.id.dogruyanliscevap);
        final TextView cevapText = (TextView) satirView.findViewById(R.id.dogruyanliscevaptext);
        final LinearLayout linearLayout = (LinearLayout) satirView.findViewById(R.id.dogruYanlisShow);

        linearLayout.setVisibility(View.GONE);
        cevapText.setVisibility(View.GONE);
        dogruCevap.setVisibility(View.GONE);

        checkdogru.setVisibility(View.GONE);
        checkyanlis.setVisibility(View.GONE);

        trueButton.setVisibility(View.VISIBLE);
        falseButton.setVisibility(View.VISIBLE);
        onay.setVisibility(View.VISIBLE);


        dogruYanlisSoru = dogruYanlisSorular.get(position);
        soruText.setText(dogruYanlisSoru.getSoruText());
        trueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                falseButton.setBackgroundColor(0xFFFFFFFF);
                trueButton.setBackgroundColor(0xFF00FFFF);
                cevap = "T";
            }
        });

        falseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trueButton.setBackgroundColor(0xFFFFFFFF);
                falseButton.setBackgroundColor(0xFF00FFFF);
                cevap = "F";
            }
        });

        onay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trueButton.setClickable(false);
                falseButton.setClickable(false);
                falseButton.setVisibility(View.VISIBLE);
                onay.setVisibility(View.GONE);
                dogruCevap.setVisibility(View.VISIBLE);
                cevapText.setVisibility(View.VISIBLE);
                Log.v("cevap", ""+cevap);
                if(dogruYanlisSoru.getDogruCevap().equals("T"))
                    dogruCevap.setText("True");
                else
                    dogruCevap.setText("False");
                if(cevap.equalsIgnoreCase(dogruYanlisSoru.getDogruCevap())){
                    checkdogru.setVisibility(View.VISIBLE);
                }else{
                    checkyanlis.setVisibility(View.VISIBLE);
                }
                new DogruYanlisCevabiEkle().execute();
            }
        });



        return satirView;
    }

    private class DogruYanlisCevabiEkle extends AsyncTask<String, Void, String> {

        int responseCode = 0;

        @Override
        protected String doInBackground(String... params) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            try {
                responseCode = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/students/" + ogrenci.getOgrenci_id() + "/dogru_yanlis_questions/" + dogruYanlisSoru.getSoruId() + "/students_dyquestions","cevap=" + cevap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("AAA", "" + responseCode);
            return "a";
        }
    }
}
