package adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.example.user.fnl496_android.Kelime;
import com.example.user.fnl496_android.KelimelerFragment;
import com.example.user.fnl496_android.Main2Activity;
import com.example.user.fnl496_android.NavigationActivity;
import com.example.user.fnl496_android.R;
import com.example.user.fnl496_android.WebRequest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;


public class OgretmeninKelimeleriAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<Kelime> kelimeler;
    Kelime word;
    int positionA =0;
    int kitap_id=0;

    public OgretmeninKelimeleriAdapter(Activity activity, ArrayList<Kelime> list,int kitapId) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        kelimeler = list;
        kitap_id = kitapId;
    }

    @Override
    public int getCount() {
        return kelimeler.size();
    }

    @Override
    public Object getItem(int position) {
        return kelimeler.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_ogretmenin_kelimeleri, null);

        TextView kelime = (TextView) satirView.findViewById(R.id.kelime);
        Button anlam = (Button) satirView.findViewById(R.id.language);
        Button anlam1 = (Button) satirView.findViewById(R.id.anlam);

        anlam.setTag(position);
        anlam1.setTag(position);

        word = kelimeler.get(position);
        kelime.setText(word.getKelimeText());

        anlam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionA=(Integer)v.getTag();
                new GetKelimeninAnlami().execute();
            }
        });

        anlam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionA=(Integer)v.getTag();
                new GetKelimeninAnlami().execute();
            }
        });

        return satirView;
    }


    private class GetKelimeninAnlami extends AsyncTask<String, Void, String> {
        String htmlStr = null;
        Kelime word;
        @Override
        protected String doInBackground(String... arg0) {
            word = kelimeler.get(positionA);
            WebRequest webreq = new WebRequest();
            try {
                htmlStr = webreq.makeWebServiceCallGet("https://www.seslisozluk.net/" + word.getKelimeText() + "-nedir-ne-demek/");
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Log.v("njnjn","kkk "+ htmlStr.length());
          /*  int maxLogSize = 4000;
            for(int i = 0; i <= htmlStr.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i+1) * maxLogSize;
                end = end > htmlStr.length() ? htmlStr.length() : end;
                Log.v("sss", htmlStr.substring(start, end));
            }*/

            //Log.i(htmlStr, "!! Json");

            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);

            final Dialog anlam_dialog = new Dialog(activity);
            anlam_dialog.setContentView(R.layout.dialog_anlam);
            anlam_dialog.show();
            anlam_dialog.getWindow().setGravity(Gravity.CENTER);

            TextView baslik = (TextView) anlam_dialog.findViewById(R.id.baslik);
            baslik.setText(word.getKelimeText());
            TextView ilkanlam = (TextView) anlam_dialog.findViewById(R.id.ilkanlam);
            TextView ikincianlam = (TextView) anlam_dialog.findViewById(R.id.ikincianlam);
            TextView cumleTurkce = (TextView) anlam_dialog.findViewById(R.id.cumleTurkce);
            TextView cumleIng = (TextView) anlam_dialog.findViewById(R.id.cumleIng);
            TextView bir = (TextView) anlam_dialog.findViewById(R.id.bir);
            TextView iki = (TextView) anlam_dialog.findViewById(R.id.iki);
            TextView ornekbaslik = (TextView) anlam_dialog.findViewById(R.id.ornekbaslik);
            TextView tr = (TextView) anlam_dialog.findViewById(R.id.tr);
            TextView eng = (TextView) anlam_dialog.findViewById(R.id.eng);
            ikincianlam.setVisibility(View.VISIBLE);
            cumleTurkce.setVisibility(View.VISIBLE);
            cumleIng.setVisibility(View.VISIBLE);
            bir.setVisibility(View.VISIBLE);
            iki.setVisibility(View.VISIBLE);
            ornekbaslik.setVisibility(View.VISIBLE);
            tr.setVisibility(View.VISIBLE);
            eng.setVisibility(View.VISIBLE);

            Button cancel = (Button) anlam_dialog.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    anlam_dialog.cancel();
                }
            });

            Document doc = Jsoup.parse(htmlStr);
            Element first = doc.select("dd").first();
            if (first == null) {
                ilkanlam.setText("Bu sözcüğün Türkçe karşılığı bulunamamıştır.");
                ikincianlam.setVisibility(View.GONE);
                cumleTurkce.setVisibility(View.GONE);
                cumleIng.setVisibility(View.GONE);
                bir.setVisibility(View.GONE);
                iki.setVisibility(View.GONE);
                ornekbaslik.setVisibility(View.GONE);
                tr.setVisibility(View.GONE);
                eng.setVisibility(View.GONE);
            } else {
                ilkanlam.setText(first.select("a").text());
                Element second = doc.select("dd").get(1);
                ikincianlam.setText(second.select("a").text());
                if (first.select("q").first() == null) {
                    ornekbaslik.setVisibility(View.GONE);
                    tr.setVisibility(View.GONE);
                    eng.setVisibility(View.GONE);
                    cumleTurkce.setVisibility(View.GONE);
                    cumleIng.setVisibility(View.GONE);
                } else {
                    cumleTurkce.setText(first.select("q").first().text());
                    cumleIng.setText(first.select("q").last().text());
                }
            }
        }
    }

}

