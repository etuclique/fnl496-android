package adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.view.Gravity;

import com.example.user.fnl496_android.Message;
import com.example.user.fnl496_android.R;

import java.util.ArrayList;

/**
 * Created by İlayda Şahiner on 08.03.2018.
 */

public class MessagesCustomAdapter extends BaseAdapter {

        Activity activity;
        LayoutInflater layoutInflater;
        ArrayList<Message> mesajlar;


        public MessagesCustomAdapter(Activity activity, ArrayList<Message> list) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.activity = activity;
            mesajlar = list;
        }

        @Override
        public int getCount() {
            return mesajlar.size();
        }

        @Override
        public Object getItem(int position) {
            return mesajlar.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View satirView;
            TextView message;
            final Message mesaj = mesajlar.get(position);
            if(mesaj.getSender()){
                satirView = layoutInflater.inflate(R.layout.list_item_messages2, null);
                message  = (TextView) satirView.findViewById(R.id.message2);
            }else{
                satirView = layoutInflater.inflate(R.layout.list_item_messages, null);
                message = (TextView) satirView.findViewById(R.id.message);
            }
            message.setText(mesaj.getBody());

            return satirView;
        }


    }


