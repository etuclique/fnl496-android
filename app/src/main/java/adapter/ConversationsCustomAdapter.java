package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.user.fnl496_android.Konusma;
import com.example.user.fnl496_android.R;

import java.util.ArrayList;

/**
 * Created by İlayda Şahiner on 08.03.2018.
 */

public class ConversationsCustomAdapter extends BaseAdapter {

        Activity activity;
        LayoutInflater layoutInflater;
        ArrayList<Konusma> konusmalar;


        public ConversationsCustomAdapter(Activity activity, ArrayList<Konusma> list) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.activity = activity;
            konusmalar = list;
        }

        @Override
        public int getCount() {
            return konusmalar.size();
        }

        @Override
        public Object getItem(int position) {
            return konusmalar.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View satirView;
            satirView = layoutInflater.inflate(R.layout.list_item_conversations,null);

            TextView createdAt = (TextView) satirView.findViewById(R.id.created);
            TextView konu = (TextView) satirView.findViewById(R.id.konusmakonu);


            final Konusma konusma = konusmalar.get(position);

            createdAt.setText(konusma.getCreated_at());
            konu.setText(konusma.getKonu());

            return satirView;
        }


    }


