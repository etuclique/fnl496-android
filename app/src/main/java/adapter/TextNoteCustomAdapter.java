package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.user.fnl496_android.Kitap;
import com.example.user.fnl496_android.R;

import java.util.ArrayList;

/**
 * Created by İlayda Şahiner on 22.02.2018.
 */

public class TextNoteCustomAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<String> notlar;


    public TextNoteCustomAdapter(Activity activity, ArrayList<String> list) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        notlar = list;

    }

    @Override
    public int getCount() {
        return notlar.size();
    }

    @Override
    public Object getItem(int position) {
        return notlar.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_textnotlar, null);


        TextView icerik = (TextView) satirView.findViewById(R.id.icerik);
        final String not = notlar.get(position);
        icerik.setText(not);

        return satirView;
    }


}

