package adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.example.user.fnl496_android.Kelime;
import com.example.user.fnl496_android.KelimelerFragment;
import com.example.user.fnl496_android.KelimelerTabbed;
import com.example.user.fnl496_android.Main2Activity;
import com.example.user.fnl496_android.NavigationActivity;
import com.example.user.fnl496_android.R;
import com.example.user.fnl496_android.WebRequest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by İlayda Şahiner on 31.01.2018.
 */

public class KelimelerCustomAdapter  extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<Kelime> kelimeler;
    Kelime word;
    int positionA =0;
    int kitap_id=0;
    int ogrenci_id=0;

    public KelimelerCustomAdapter(Activity activity, ArrayList<Kelime> list,int kitapId,int ogrenciId) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        kelimeler = list;
        kitap_id = kitapId;
        ogrenci_id=ogrenciId;
    }

    @Override
    public int getCount() {
        return kelimeler.size();
    }

    @Override
    public Object getItem(int position) {
        return kelimeler.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_kelimeler, null);

        TextView kelime = (TextView) satirView.findViewById(R.id.kelime);
        Button anlam = (Button) satirView.findViewById(R.id.language);
        Button anlam1 = (Button) satirView.findViewById(R.id.anlam);
        Button sil = (Button) satirView.findViewById(R.id.sil);
        sil.setTag(position);
        anlam.setTag(position);
        anlam1.setTag(position);

        word = kelimeler.get(position);
        kelime.setText(word.getKelimeText());

        anlam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionA=(Integer)v.getTag();
                new GetKelimeninAnlami().execute();
            }
        });

        anlam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionA=(Integer)v.getTag();
                new GetKelimeninAnlami().execute();
            }
        });

        sil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionA=(Integer)v.getTag();
                new DeleteKelime().execute();
            }
        });





        return satirView;
    }

    private class DeleteKelime extends AsyncTask<String, Void, String> {
        int responseCode = 0;
        Kelime word;
        @Override
        protected String doInBackground(String... arg0) {
            word = kelimeler.get(positionA);
            WebRequest webreq = new WebRequest();

            try {
                responseCode = webreq.makeWebServiceCallDelete("https://teacherclique.herokuapp.com/api/students/"+ogrenci_id+"/books/" + kitap_id + "/student_chosen_words/"+word.getKelime_id());
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("aaaaa", ""+responseCode);
            //    kitaplar= ParseJsonForBook(jsonStr);
            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);
            if (responseCode == 204) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                alertDialogBuilder.setTitle("Kelimenin Listeden Silinmesi");
                alertDialogBuilder.setMessage(word.getKelimeText()+" kelimesi listeden silinmiştir.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        activity.finish();
                        Intent i = new Intent(activity,KelimelerTabbed.class);
                        i.putExtra("id_of_clicked_book",kitap_id);
                        activity.startActivity(i);
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            }
            else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                alertDialogBuilder.setTitle("Kelimenin Listeden Silinmesi");
                alertDialogBuilder.setMessage("Kelimeniz listeden silenememiştir.");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4C87F4"));
                    }
                });
                alertDialog.show();
            }
        }

    }


    private class GetKelimeninAnlami extends AsyncTask<String, Void, String> {
        String htmlStr = null;
        Kelime word;
        @Override
        protected String doInBackground(String... arg0) {
            word = kelimeler.get(positionA);
            WebRequest webreq = new WebRequest();
            try {
                htmlStr = webreq.makeWebServiceCallGet("https://www.seslisozluk.net/" + word.getKelimeText() + "-nedir-ne-demek/");
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Log.v("njnjn","kkk "+ htmlStr.length());
          /*  int maxLogSize = 4000;
            for(int i = 0; i <= htmlStr.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i+1) * maxLogSize;
                end = end > htmlStr.length() ? htmlStr.length() : end;
                Log.v("sss", htmlStr.substring(start, end));
            }*/

            //Log.i(htmlStr, "!! Json");

            return "a";

        }


        @Override
        protected void onPostExecute(String requestresult) {
            super.onPostExecute(requestresult);

            final Dialog anlam_dialog = new Dialog(activity);
            anlam_dialog.setContentView(R.layout.dialog_anlam);
            anlam_dialog.show();
            anlam_dialog.getWindow().setGravity(Gravity.CENTER);

            TextView baslik = (TextView) anlam_dialog.findViewById(R.id.baslik);
            baslik.setText(word.getKelimeText());
            TextView ilkanlam = (TextView) anlam_dialog.findViewById(R.id.ilkanlam);
            TextView ikincianlam = (TextView) anlam_dialog.findViewById(R.id.ikincianlam);
            TextView cumleTurkce = (TextView) anlam_dialog.findViewById(R.id.cumleTurkce);
            TextView cumleIng = (TextView) anlam_dialog.findViewById(R.id.cumleIng);
            TextView bir = (TextView) anlam_dialog.findViewById(R.id.bir);
            TextView iki = (TextView) anlam_dialog.findViewById(R.id.iki);
            TextView ornekbaslik = (TextView) anlam_dialog.findViewById(R.id.ornekbaslik);
            TextView tr = (TextView) anlam_dialog.findViewById(R.id.tr);
            TextView eng = (TextView) anlam_dialog.findViewById(R.id.eng);
            ikincianlam.setVisibility(View.VISIBLE);
            cumleTurkce.setVisibility(View.VISIBLE);
            cumleIng.setVisibility(View.VISIBLE);
            bir.setVisibility(View.VISIBLE);
            iki.setVisibility(View.VISIBLE);
            ornekbaslik.setVisibility(View.VISIBLE);
            tr.setVisibility(View.VISIBLE);
            eng.setVisibility(View.VISIBLE);

            Button cancel = (Button) anlam_dialog.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    anlam_dialog.cancel();
                }
            });

            Document doc = Jsoup.parse(htmlStr);
            Element first = doc.select("dd").first();
            if (first == null) {
                ilkanlam.setText("Bu sözcüğün Türkçe karşılığı bulunamamıştır.");
                ikincianlam.setVisibility(View.GONE);
                cumleTurkce.setVisibility(View.GONE);
                cumleIng.setVisibility(View.GONE);
                bir.setVisibility(View.GONE);
                iki.setVisibility(View.GONE);
                ornekbaslik.setVisibility(View.GONE);
                tr.setVisibility(View.GONE);
                eng.setVisibility(View.GONE);
            } else {
                ilkanlam.setText(first.select("a").text());
                Element second = doc.select("dd").get(1);
                ikincianlam.setText(second.select("a").text());
                if (first.select("q").first() == null) {
                    ornekbaslik.setVisibility(View.GONE);
                    tr.setVisibility(View.GONE);
                    eng.setVisibility(View.GONE);
                    cumleTurkce.setVisibility(View.GONE);
                    cumleIng.setVisibility(View.GONE);
                } else {
                    cumleTurkce.setText(first.select("q").first().text());
                    cumleIng.setText(first.select("q").last().text());
                }
            }
        }
    }

}

