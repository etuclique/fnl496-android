package adapter;
import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.example.user.fnl496_android.Main2Activity;
import com.example.user.fnl496_android.R;

import java.util.ArrayList;

/**
 * Created by İlayda Şahiner on 15.02.2018.
 */

public class CustomPagerAdapter extends PagerAdapter{

    LayoutInflater mLayoutInflater;
    ArrayList<String> pages;
    Activity activity;

    public CustomPagerAdapter(Activity activity, ArrayList<String> pages) {
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.pages = pages;
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.page_item, container, false);
        itemView.setTag("View"+position);
        WebView mWebView = (WebView) itemView.findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setLongClickable(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.loadData(pages.get(position), "text/html; charset=utf-8", "UTF-8");
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
