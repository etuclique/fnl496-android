package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.fnl496_android.Ogrenci;
import com.example.user.fnl496_android.R;
import com.example.user.fnl496_android.WebRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;

import objects.SecmeliSoru;


/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class SecmeCustomAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<SecmeliSoru> secmeSorular;
    String cevap="";
    Ogrenci ogrenci;
    SecmeliSoru secmeliSoru;

    public SecmeCustomAdapter(Activity activity, ArrayList<SecmeliSoru> list) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        secmeSorular = list;

        Gson gson = new Gson();
        SharedPreferences prefs = activity.getSharedPreferences("OgrenciData", Context.MODE_PRIVATE);
        String ogrenciBilgisi = prefs.getString("ogrenci", "bulunamadi");
        ogrenci = gson.fromJson(ogrenciBilgisi, new TypeToken<Ogrenci>() {
        }.getType());

    }

    @Override
    public int getCount() {
        return secmeSorular.size();
    }

    @Override
    public Object getItem(int position) {
        return secmeSorular.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_secme, null);

        final Button soruAButton = (Button) satirView.findViewById(R.id.soruAButton);
        final Button soruBButton = (Button) satirView.findViewById(R.id.soruBButton);
        final Button soruCButton = (Button) satirView.findViewById(R.id.soruCButton);
        final Button soruDButton = (Button) satirView.findViewById(R.id.soruDButton);
        final Button onay = (Button) satirView.findViewById(R.id.onay);
        final ImageView checkdogru = (ImageView) satirView.findViewById(R.id.checkdogru);
        final ImageView checkyanlis = (ImageView) satirView.findViewById(R.id.checkyanlis);
        final TextView dogruCevap = (TextView) satirView.findViewById(R.id.cevap);
        final TextView cevapText = (TextView) satirView.findViewById(R.id.cevaptext);
        final TextView secilen = (TextView) satirView.findViewById(R.id.secilen);
        TextView soruAText = (TextView) satirView.findViewById(R.id.soruAText);
        TextView soruBText = (TextView) satirView.findViewById(R.id.soruBText);
        TextView soruCText = (TextView) satirView.findViewById(R.id.soruCText);
        TextView soruDText = (TextView) satirView.findViewById(R.id.soruDText);
        TextView soruTextSecme = (TextView) satirView.findViewById(R.id.soruTextSecme);
        LinearLayout secmeShow = (LinearLayout) satirView.findViewById(R.id.secmeShow);

        soruBButton.setClickable(true);
        soruCButton.setClickable(true);
        soruDButton.setClickable(true);
        soruAButton.setClickable(true);
        onay.setVisibility(View.VISIBLE);
        secilen.setVisibility(View.VISIBLE);
        secmeShow.setVisibility(View.GONE);
        checkdogru.setVisibility(View.GONE);
        checkyanlis.setVisibility(View.GONE);
        dogruCevap.setVisibility(View.GONE);
        cevapText.setVisibility(View.GONE);

        secmeliSoru = secmeSorular.get(position);

        soruTextSecme.setText(secmeliSoru.getSoruText());
        soruAText.setText(String.valueOf(secmeliSoru.getaText()));
        soruBText.setText(String.valueOf(secmeliSoru.getbText()));
        soruCText.setText(String.valueOf(secmeliSoru.getcText()));
        soruDText.setText(String.valueOf(secmeliSoru.getdText()));

        soruAButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soruBButton.setBackgroundColor(0xFFFFFFFF);
                soruCButton.setBackgroundColor(0xFFFFFFFF);
                soruDButton.setBackgroundColor(0xFFFFFFFF);
                soruAButton.setBackgroundColor(0xFF00FFFF);
                cevap="A";
                secilen.setText("A");
            }
        });

        soruBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soruAButton.setBackgroundColor(0xFFFFFFFF);
                soruCButton.setBackgroundColor(0xFFFFFFFF);
                soruDButton.setBackgroundColor(0xFFFFFFFF);
                soruBButton.setBackgroundColor(0xFF00FFFF);
                cevap="B";
                secilen.setText("B");
            }
        });
        soruCButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soruAButton.setBackgroundColor(0xFFFFFFFF);
                soruBButton.setBackgroundColor(0xFFFFFFFF);
                soruDButton.setBackgroundColor(0xFFFFFFFF);
                soruCButton.setBackgroundColor(0xFF00FFFF);
                cevap ="C";
                secilen.setText("C");
            }
        });
        soruDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soruAButton.setBackgroundColor(0xFFFFFFFF);
                soruBButton.setBackgroundColor(0xFFFFFFFF);
                soruCButton.setBackgroundColor(0xFFFFFFFF);
                soruDButton.setBackgroundColor(0xFF00FFFF);
                cevap ="D";
                secilen.setText("D");
            }
        });

        onay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("Soruid: ",""+secmeliSoru.getSoruId());
                Log.v("cevap", ""+cevap);
                soruBButton.setClickable(false);
                soruCButton.setClickable(false);
                soruDButton.setClickable(false);
                soruAButton.setClickable(false);
                onay.setVisibility(View.GONE);
                dogruCevap.setVisibility(View.VISIBLE);
                cevapText.setVisibility(View.VISIBLE);
                dogruCevap.setText(secmeliSoru.getDogruCevap());
                if(cevap.equals(secmeliSoru.getDogruCevap())){
                    checkdogru.setVisibility(View.VISIBLE);
                }else{
                    checkyanlis.setVisibility(View.VISIBLE);
                }
                new SecmeliCevabiEkle().execute();
            }
        });
        return satirView;
    }

    private class SecmeliCevabiEkle extends AsyncTask<String, Void, String> {
        int responseCode = 0;

        @Override
        protected String doInBackground(String... params) {

            WebRequest webreq = new WebRequest();
            String jsonStr = null;
            Log.v("cevap", ""+cevap);
            try {
                responseCode = webreq.makeWebServiceCallPostWithResponseCode("https://teacherclique.herokuapp.com/api/students/"+ogrenci.getOgrenci_id()+"/secmeli_questions/"+secmeliSoru.getSoruId()+"/students_secquestions","cevap=" + cevap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("AAA", ""+responseCode);
            return "a";
        }
    }

}
