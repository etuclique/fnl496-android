package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.user.fnl496_android.Kitap;
import com.example.user.fnl496_android.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by feyzaiyi on 22.12.2017.
 */

public class KitaplarCustomAdapter extends BaseAdapter {

    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<Kitap> kitaplar;


    public KitaplarCustomAdapter(Activity activity, ArrayList<Kitap> list) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        kitaplar = list;
    }

    @Override
    public int getCount() {
        return kitaplar.size();
    }

    @Override
    public Object getItem(int position) {
        return kitaplar.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View satirView;
        satirView = layoutInflater.inflate(R.layout.list_item_kitaplar, null);


        TextView kitapBaslik = (TextView) satirView.findViewById(R.id.kitapBaslik);
        TextView kitapYazari = (TextView) satirView.findViewById(R.id.kitapYazari);
        TextView kitapSayfaNo = (TextView) satirView.findViewById(R.id.kitapSayfaNo);
        TextView kitapOzet = (TextView) satirView.findViewById(R.id.kitapOzet);
        ImageView kapak = (ImageView) satirView.findViewById(R.id.kitapFoto);

        final Kitap kitap = kitaplar.get(position);

/*
        BitmapDrawable background =  new BitmapDrawable(activity.getResources(), kitap.getKapak_resim());
        kapak.setBackground(background);*/

        kitapBaslik.setText(kitap.getKitap_adi());
        kitapYazari.setText(kitap.getKitap_yazari());
        kitapSayfaNo.setText(String.valueOf(kitap.getKitapSayfaNo()));
        kitapOzet.setText(kitap.getKitap_özet());

        return satirView;
    }



}
